/*******************
 * C Peculiarities *
 *******************/

#include <assert.h>
#include <stdlib.h>

/*   ____________________
 * _| User-defined types |__________________________________________________ */

/* struct, enum and union identifiers have a different namespace from type
 * identifiers
 */

struct point {  /* `point` is a tag, not a type name */
    int x;
    int y;
};
typedef struct point point_t;  /* assign a type name to a tag */

typedef struct {  /* tags are optional */
    int x;
    int y;
} point2_t;

struct point point1;  /* declaration with a tag */
point_t point2;  /* declaration with a name */

typedef enum {
    /* values are optional */
    RED = 0xFF3300,
    AMBER = 0xFFFF00,
    GREEN = 0x33CC33,
} colour_t;

colour_t colour = RED;

typedef union {
    /* `bits` are continuous in memory */
    struct {
        unsigned char bit1:1;
        unsigned char bit2:1;
        unsigned char bit3:1;
        unsigned char bit4:1;
        unsigned char unused:4;
    } bits;
    /* `word` starts at the same memory location as `bits` */
    unsigned char word;
} bit_field_t;

void use_bit_field()
{
    bit_field_t field;
    field.word = 0b11010000;
    assert(field.bits.bit2 == 1);
}

/*   __________
 * _| Pointers |____________________________________________________________ */

void use_pointers()
{
    point_t points[3];
    point_t *p_point;
    point_t *p_points[3];
    int i = 1;

    assert(points[i] == *(points + i));
    assert(p_point->x == (*p_point).x);
    assert(points[i].x == (*(points + i)).x);
    assert(p_points[i]->x == (*(*(p_points + i))).x);
}

/*   ___________________
 * _| Function pointers |___________________________________________________ */

int add_two_ints(int x, int y)
{
    return x + y;
}

void use_function_pointers()
{
    int (*p_func)(int,int);

    p_func = &add_two_ints;
    assert((*p_func)(2, 3) == 5);
}

/*   ___________________
 * _| Memory allocation |___________________________________________________ */

void allocate_memory()
{
    point_t *points;

    points = (point_t *)malloc(8 * sizeof(point_t));  /* Allocate */
    points = (point_t *)realloc(points, 16 * sizeof(point_t));  /* Resize */
    free(points);

    points = (point_t *)calloc(12, sizeof(point_t));  /* Allocate and clear */
    assert(points[2].x == 0);
    free(points);
}

/* _________________________________________________________________________ */

int main(int argc, char *argv[])
{
    use_bit_field();
    use_pointers();
    use_function_pointers();
    allocate_memory();
    return(0);
}
