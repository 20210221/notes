# Reasoning Notes

## Methods of reasoning

- __Testimony__: accepting someone's words as evidence
- __Observation__: acquiring information through one's senses
- __Argument__: a series of propositions (the _predicates_) in support of a
  claim (the _conclusion_)

## Elements of arguments

- __Truth value__: the state of being true or false (or indeterminate)
- __Assertion__: a linguistic act that has a truth value
- __Proposition__: the content of an assertion, i.e. a statement that has truth
  value
- __Predicate__: a statement containing one or more variables; if truth values
  are assigned to all the variables in a predicate, the resulting statement is a
  proposition

## Argument types

- __Deductive__: if all premises are true and there is entailment then the
  conclusion is necessarily true
    - __Detachment / Modus Ponens__: _P → Q, P ⊨ Q_
    - __Syllogism / Modus Barbara__: _P → Q, Q → R ⊨ P → R_
    - __Contrapositive / Modus Tollens__: _P → Q, ¬Q ⊨ ¬P_
    - __Modus Tollendo Ponens__: _P ∨ Q, ¬P ⊨ Q_
- __Inductive__: premises provide strong evidence to predict a conclusion that
  is likely to be true
- __Abductive__: conclusion isn't based on evidence but on eliminating possible
  explanations until the most plausible explanation is left
- __Non sequitur__: "does not follow"; a conclusion is erroneously derived from
  premises
    - __Affirming the consequent__: _P → Q, Q ⊨ P_
    - __Denying the antecedent__: _P → Q, ¬P ⊨ ¬Q_

## Fallacies

- __Faulty generalisation__: reaching a conclusion from weak premises
    - __Fallacy of accident__: ignoring an exception to a general claim
    - __Cherry-picking__: presenting the cases that confirm a claim while
      ignoring the ones that contradict it
    - __Anecdotal evidence__: using an isolated example to prove a general
      statement
    - __Bandwagon__: assuming that if many people believe something then it's
      true
    - __Middle ground__: assuming that a compromise between two extremes must be
      the truth
    - __Gambler's fallacy__: assuming causation between independent events
    - __False cause__: assuming that correlation implies causation
    - __Slippery slope__: assuming that if we allow A to happen then it will
      eventually lead to B, therefore we shouldn't allow A to happen
    - __Black-or-white__: presenting two alternatives as the only possibilities
      while many more exist
- __Red herring__: reaching a conclusion from deliberately distracting premises
    - __Ad hominem__: attacking the arguer instead of the argument
    - __Appeal to authority__: assuming that if someone with great reputation
      claims something then it's true
    - __Strawman__: deliberately misrepresenting someone's argument
    - __Appeal to emotion__: an emotionally manipulative response instead of a
      valid argument
    - __Tu quoque__ (you too): answering criticism with criticism instead of a
      counterargument
    - __Missing the point__: a valid argument that doesn't address the issue in
      question
    - __Personal incredulity__: assuming that because something is difficult to
      understand, it's probably false
    - __Loaded question__: a question that can only be answered one way without
      appearing guilty
    - __Begging the question__: an argument in which the premise already assumes
      that the conclusion is true
- __Ambiguity fallacy__: applying double meaning to misrepresent the truth
    - __Equivocation__: using multiple different meanings of a word in the
      premises, leading to a false conclusion
    - __Fallacy of composition__: assuming that if something is true for one
      part then it's true for the whole
    - __Fallacy of division__: assuming that if something is true for the whole
      then it must be true for all parts
- __Psychologist's fallacy__: assuming that a subjective experience is generally
  true
    - __Special pleading__: making up exceptions to a rule that cannot be proved
      or disproved
    - __Burden of proof__: assuming that because a claim cannot be either proved
      or disproved by anyone then it must be true
