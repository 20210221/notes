/***************************
 * JavaScript New Features *
 ***************************/

/*** ECMAScript 6 (ES2015) ***/

// Module scoping (similar to `require`/`module.exports` in Node.js)
import { assert } from 'assert';
export function publicFunction() { return 'foo'; }
export var publicVariable = 'bar';

// Lambda functions
var squares = [1, 2, 3].map((x) => x*x);
squares.forEach((x, i) => assert(x === [1, 4, 9][i]));

// In regular functions `this` is bound to the context the function is called with
function createObjectWithoutArrowFunction() {
    this.foo = 21;
    return {
        foo: 42,
        getFoo: function() {
            return this.foo;
        },
    };
}
assert(createObjectWithoutArrowFunction().getFoo() === 42);

// Arrow functions don't have their own `this` and `arguments` bindings
function createObjectWithArrowFunction() {
    this.foo = 21;
    return {
        foo: 42,
        getFoo: () => this.foo,
    };
}
assert(createObjectWithArrowFunction().getFoo() === 21);  // `this` from the outer context

// Spread operator (like `*args` in Python)
var arr1 = [2, 3];
assert([1, ...arr1][2] === 3);

// Literal string interpolation (like `#{var}` in Ruby)
var w = 'world';
assert(`Hello ${w}` === 'Hello world');

// For loop over values
for (var i in ['x']) {  // `in` iterates over keys
    for (var j of ['a']) {  // `of` iterates over values
        assert(i === 0 && j === 'a');
    }
}

// Generators (just like in Python)
function* squares(low, high) {
    while (low <= high) {
        yield Math.pow(low++, 2);
    }
}
for (var n of squares(1, 5)) {
    console.log(n);
}

// Constants
const c = 'c';
try {
    c = 'd';
} catch(err) {
    assert(err instanceof TypeError);
}

// Variable scopes (like `def` and `let` in Clojure)
var x = 1;  // function-local scope
if (true) {
    let x = 2;  // block-local scope
    assert(x === 2);
}
assert(x === 1);

// Proper classes
class Parent {
    constructor(a) {
        this._a = a;
    }

    // Getter/setter
    get a() { return this._a.toUpperCase(); }
    set a(value) { this._a = value; }
}

class Child extends Parent {
    constructor(a, b) {
        super(a);
        this._b = b;
    }
}

var instance = new Child('a', 'b');
assert(instance.a === 'A');
assert(instance._b === 'b');  // Still no information hiding (like in Python)

// Promises
function getRandomOddNumber() {
    return new Promise((resolve, reject) => {
        let number = parseInt(Math.random() * 100);  // A slow operation
        if (number % 2 === 1) {
            resolve(number);
        }
        else {
            reject('Not accepted');
        }
    });
}
function getSquare(number) {
    return new Promise((resolve, reject) => resolve(number * number));
}

// Chained together
var promise = getRandomOddNumber()
    .then(getSquare)  // the `resolve` callback of `getRandomOddNumber`
    .then(console.log)  // the `resolve` callback of `getSquare`
    .catch(console.log);  // the `reject` callback of either

// It isn't possible to dereference a future value
// (there is no blocking operation; everything is handled via callbacks)
var value;
promise.then((result) => value = result);


/*** ECMAScript 7 (ES2016) ***/

// Array membership check
assert([1, 2, 3].includes(2));


/*** ECMAScript 8 (ES2017) ***/

// Async functions return promises
async function getFoo() {
    return 'foo';
}
getFoo().then((result) => assert(result == 'foo'));

// Is equivalent to
function getFoo2() {
    return Promise.resolve('foo');
}

// As an arrow function
const getFoo3 = async () => 'foo';

// Await dereferences a promise; only works inside an async function
async () => {
    let getBar = new Promise((resolve) => resolve('bar'));
    return await Promise.all([getFoo(), getBar()]).join('');
}().then(
    (result) => assert(result == 'foobar')
);


/*** ECMAScript 9 (ES2018) ***/

// Array destructuring
var arr2 = ["A", "B", "C"];
var [first, second] = arr2;
assert(first === "A" && second === "B");
var [first, , third] = arr2;  // ',' skips elements
assert(first === "A" && third === "C");

// Object destructuring
var obj = {one: 1, two: 2, three: 3};
var {one, three} = obj;
assert(one === 1 && three === 3);


/*** ECMAScript 11 (ES2020) ***/

// Private members in classes with `#`
class InfoHider {
    #foo = 5;

    #getFoo() {
        return this.#foo;
    }
}

// Conditional chaining with `?.`
const empty = {};
try {
    console.log(empty.foo.bar);
} catch(err) {
    assert(err instanceof TypeError);
}
assert(empty.foo?.bar === undefined);
