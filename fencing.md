# Fencing Notes

## Attributes of garde positions

- __High/low__: by direction relative to the horizontal fencing line
- __Inside/outside__: by direction relative to the vertical fencing line
- __Supinated/pronated__: by rotation of the wrist

## Main (supinated) garde positions

- __Quarte (4)__: High and to the inside
- __Sixte (6)__: High and to the outside
- __Septime (7)__: Low and to the inside
- __Octave (8)__: Low and to the outside

## Parries

- __Lateral__: 6 to 4
- __Circular__: 6 to 6
- __Diagonal__: 6 to 7
- __Semicircular__: 6 to 8

## Targets

- __Near__: front arm and leg
- __Middle__: trunk and head
- __Far__: back arm and leg

## Attributes of attacks

- __Direct__: starts and finishes in the same line
- __Indirect__: starts and finishes in different lines
- __Simple__: doesn't involve feints (faking an attack)
- __Compound__: involves feints (faking an attack)

## Lunges

- __Explosive__: for simple attacks
- __Accelerating__: for compound attacks, slow during the initial feint
- __Step and lunge__: step forward for the feint and lunge for the finishing
  attack
- __Ballestra__: the lunge is preceded by a jump to close the distance
- __Flèche__: attack with a sprint instead of a lunge

## Attacks

- __Straight attack (absence de fer)__: blades don't touch
- __Beat__: hit your opponent's blade to get it out of the way
- __Glide (coulé)__: slide your blade forward along your opponent's
- __Flick__: hit your opponent's near target with a flick of your wrist
- __Taking the blade (pris de fer)__: push your opponent's blade to a different
  line
- __Disengage__: change line under your opponent's blade with a circular motion
  to get from an uncovered position to a covered position
- __Cutover (coupé)__: change line over your opponent's blade by turning your
  wrist
- __Broken time attack__: pull your arm back to avoid the parry, then extend
  again
- __Beat-disengage__: beat, wait for your opponent to beat back and avoid their
  blade
- __Counter-disengage__: avoid your opponent's blade when they disengage
- __One-two attack__: disengage followed by counter-disengage

## Phrases

- __Parry-riposte__: A attacks, B defends and counterattacks
- __Stop hit__: A attacks, B attacks A's near targets
- __Remise__: A attacks, B defends, A stays in the lunge and repeats the attack
- __Reprise__: A attacks, B defends, A recovers and attacks again

## Preparations for attack

- Close the distance
- Change rhythm
- Close the distance with your near targets exposed to provoke an attack
- __Apel__: stomp with your front leg to distract your opponent

## Exploring your opponent (reconnaissance)

- Feel of the blade (sentiment de fer)
    - _Light grip_: glide attack
    - _Strong grip_: disengage attack
- Reaction to beat
    - _Opens to beat_: direct beat attack
    - _Beats back_: beat-disengage attack
- Grip position
    - _Normal/high_: disengage
    - _Low_: cutover

## Tips

- Control the point with your fingers, not your wrist or arm
- Keep eye contact with your opponent
- Start attacks with extension
- Keep your centre of gravity low
- Keep the distance
- Mind your garde position to keep your lower arm protected
- Go for the blade before starting an attack
- Always direct the point of the blade towards the target
- Keep your hand higher than the point when your arm is extended
- Do explosive lunges as if someone was pulling your sword
