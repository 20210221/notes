# Poetry Notes

- Daily language and literature differ in _intent_ and _intensity_ (Mary Oliver)
- __Verse__: the combination of meter, line length and rhyming pattern
- __Diction__: the choice of words in a piece of writing

## Meter

- __Scansion__: reading a poem in metrical pattern
- __Inflection__: alteration of light and heavy stresses in a word
- __Foot__: a group of syllables consisting of an emphasised syllable surrounded
  by light syllable(s)
- __Iamb / iambic foot__: a light stress followed by a heavy stress `˘ –`
- __Trochee / trochaic foot__: a heavy stress followed by a light stress `– ˘`
- __Dactyl / dactylic foot__: a heavy stress followed by two light stresses
  `– ˘ ˘`
- __Anapest / anapestic foot__: two light stresses followed by a heavy stress
  `˘ ˘ –`
- __Spondee / spondaic foot__: two equal heavy stresses `– –`
- __Pyrrhic foot__: two light stresses `˘ ˘`; only used before a spondee
- __Catalexis / catalectic foot__: truncation of the final foot of a line
- __Caesura__: pause within a line, indicated by a punctuation mark; not counted
  in scansion

## Line

- __Trimeter__: a 3-foot line
- __Tetrameter__: a 4-foot line
- __Pentameter__: a 5-foot line
- __Hexameter__: a 6-foot line
- __End-stopped / self-enclosed line__: ends with the grammatical and logical
  completion of a phrase
- __Run-on line / enjambment__: a phrase continues over the line ending into the
  next line without a break
- __Stanza__: a group of lines separated by an extra amount of space
- __Paragraph__: a group of lines whose beginning is indicated by indentation
- __Quatrain__: a 4-line metrical stanza

## Rhyme

- __Rhyme__: repetition of sounds at the end of line
- __True / perfect rhyme__: the words rhyme using exactly the same vowels
- __Slant / imperfect rhyme__: the rhyming words have similar but not identical
  vowels
- __Masculine rhyme__: only the final syllables rhyme and they have heavy stress
- __Feminine rhyme__: multiple syllables rhyme and the final syllables have
  light stress
- __Couplet__: two consecutive rhyming lines in the same meter `a,a`
- __Triplet__: three consecutive rhyming lines in the same meter `a,a,a`

## Form

- __Blank verse__: iambic pentameter without rhyme (e.g. Shakespeare's plays)
- __Free verse__: non-metrical verse
- __Stress verse / accentual verse__: metrical verse that has no predominant
  meter
- __Syllabic verse__: the number of syllables in each line is strictly repeated
  in every stanza
- __Sonnet__: a 14-line-long poem in iambic pentameter with strict rhyming
  patterns
- __Petrarchan sonnet__: _a,b,b,a a,b,b,a | c,d,d c,e,e_  or _c,d,e c,d,e_
  or _c,d,c d,c,d_
- __Shakespearean sonnet__: _a,b,a,b c,d,c,d e,f,e,f g,g_
- __Ballad__: a poem with 4-line stanzas; lines 1 and 3 are in tetrameter and 2
  and 4 are in trimeter
- __Terza rima__: a poem with 3-line stanzas in iambic pentameter and pattern
  _a,b,a b,c,b c,d,c ..._

## Sound

- __Vowels__: _a,e,i,o,u(,w,y)_
- __Semivowels__: can be sounded without a vowel: _f,h,j,l,m,n,r,s,v,w,x,y,z_
- __Mutes__: block airflow and therefore cannot be sounded without a vowel:
  _b,c,d,g,k,p,q,t_
- __Liquids__: fluent semivowels: _l,m,n,r_

## Diction

- __Alliteration__: repetition of initial sounds of words in a line
- __Assonance__: repetition of vowels within words in a line
- __Onomatopoeia__: a word that represents what it defines through its sound
  (e.g. buzz)
- __Palindrome__: a word or phrase that is the same when read forwards than read
  backwards (e.g. madam)
- __Acrostic__: the first letter of each line spells out a message

## Imagery

- __Figure__: a familiar thing linked to something unknown or difficult to
  describe
- __Texture__: sensory details that make the reader experience a figure
- __Simile__: explicit comparison that uses “like” or “as” to link the subjects
- __Metaphor__: implicit comparison
- __Personification__: an inanimate object is given personal characteristics
  (e.g. the sky is crying)
- __Conceit__: an extended metaphor that continues through the entire poem
- __Allusion__: reference to something generally known

## Sensory associations

- Taste
- Smell
- Touch
- Hearing
- Sight
- A sense of motion

## Rhetoric

- __Parallel structure__: keeping the same grammatical structure in successive
  phrases or sentences
- __Antithesis__: contrast between parts of a parallel structure, e.g. "Man
  _proposes_, God _disposes_."
- __Chiasmus__: A-B-B-A word order, e.g. "_Fair_ is _foul_, and _foul_ is
  _fair_."
- __Anaphora__: repeating the same phrase at the beginning of each part, e.g.
  "_We shall_ fight in the fields and in the streets, _we shall_ fight in the
  hills. _We shall_ never surrender."
- __Epistrophe__: repeating the same phrase at the end of each part, e.g.
  "Government of _the people_, by _the people_, for _the people_, shall not
  perish from the earth."
- __Symploce__: the combination of anaphora and epistrophe for added emphasis,
  e.g. "_We will make America_ proud _again_. _We will make America_ safe
  _again_. And yes, together _we will make America_ great _again_."
- __Asyndeton__: conjunctions (_and_, _but_, etc.) are omitted from between
  parts, e.g. "I came, I saw, I conquered."
- __Climax__: phrases are arranged in order of increasing importance, e.g.
  "There are three things that will endure: _faith_, _hope_, and _love_."

## Tools

- Tell a _story_
- Describe an everyday scene, noticing details one wouldn't normally notice
- Add a subtle action that gives the scene tension
- Draw a conclusion from the scene
- Move from life-like to symbolic gradually
- Use everyday objects and places people are familiar with as metaphors
- Talk about your life experience and weaknesses readers can identify with
- Emphasise the time dimension
- Use unfinished sentences or deliberately omit keywords to keep an idea open
- Add elements of surprise, profanity or shock to change the dynamics
- Ask _rhetorical questions_
- Apply _suspension_: reveal key details only in the closing section
- Find your recurring "signature" symbols
