# Bash Peculiarities

## Special identifiers

- `;&`: fall through to the next branch of `case`
- `{ }`: run command group in the same shell
- `( )`: run command group in a new subshell
- `%1`, `%2`, ...: IDs of active jobs
- `$''`: C-like escape sequences, e.g. `line_separator=$'\r\n'`

## Pre-defined variables

- `$1`, `$2`, ...: arguments one by one
- `$@` vs. `$*`: array of arguments, individually quoted vs. unquoted
- `$#`: the number of arguments
- `$?`: exit code of the last executed command
- `$!`: PID of the last executed command
- `$$`: PID of the shell
- `$IFS`: internal field separator for loops (default: \n)
- `$PIPESTATUS`: array of exit codes of each process in a pipeline

## Redirections

### Pipes

- `|`: pipe a command's standard output to the next command's standard input
- `|&`: pipe a command's standard error to the next command's standard input

### Redirects

- `>`: redirect standard output from the console to a file, create new
- `>>`: redirect standard output from the console to a file, append to existing
- `<`: redirect standard input from the console to a file
- `x>&y`: redirect the output of file descriptor x to file descriptor y
- `y<&x`: redirect the input of file descriptor x to file descriptor y
- `< file`: contents of 'file' to standard input
- `<(command)`: standard output of 'command' to a new file descriptor;
- print the path of the file descriptor to standard output
- `< <(command)`: standard output of 'command' to standard input
- `<<<string`: string to standard input
- `<<word`: here-document to standard input, terminated by 'word'
- `<<-word`: as above but trim leading tab characters in each line

## Arrays

### Creation

- `declare -a`: indexed array
- `declare -A`: associative array
- `arr=(1, 2)`: initialise
- `arr+=(3, 4)`: insert

### Accessing elements

- `${arr[@]}`: all elements, individually quoted
- `${arr[*]}`: all elements, unquoted
- `${#arr[@]}`: number of elements
- `${!arr[@]}`: all keys (or indexes)

## String manipulation

- `${#var}`: length of $var
- `${var:2}`: substring of $var starting at index 2
- `${var:2:3}`: 3-character-long substring of $var starting at index 2
- `${var#x}`: delete the shortest match of 'x' from the front of $var
- `${var##x}`: delete the longest match of 'x' from the front of $var
- `${var%x}`: delete the shortest match of 'x' from the back of $var
- `${var%%x}`: delete the longest match of 'x' from the back of $var
- `${var/x/y}`: replace the first 'x' in $var with 'y'
- `${var//x/y}`: replace all 'x'es in $var with 'y's
