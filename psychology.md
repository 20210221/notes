# Psychology Notes

## Psyche (Freud)

- Id: the instinctive mind (the source of needs, wants, desires and impulses)
- Ego: the rational mind (the perception of "self")
- Super-ego: the moral mind (the internalisation of cultural rules)

## Hierarchy of needs (Maslow)

1. Physiological: food, shelter, sex
2. Safety: personal, financial, health
3. Love & belonging: family, friendship, intimacy
4. Esteem: respect, self-confidence, freedom
5. Self-actualisation: altruism, spirituality

## Five core needs (Koch)

- Security (Who can I trust?)
- Identity (Who am I?)
- Belonging (Who wants me?)
- Purpose (Why am I alive?)
- Competence (What do I do well?)

## Hierarchy of logical levels (Dilt)

1. Environment
2. Behaviour (What?)
3. Competence (How?)
4. Beliefs (Why?)
5. Identity (Who am I?)

## Stages of psychological development (Erikson)

- Infancy: Hope (basic trust vs. mistrust)
- Early childhood: Will (autonomy vs. shame and doubt)
- Preschool age: Purpose (initiative vs. guilt)
- School age: Competence (industry vs. inferiority)
- Adolescence: Fidelity (identity vs. role confusion)
- Early adulthood: Love (intimacy vs. isolation)
- Adulthood: Care (generativity vs. stagnation)
- Mature age: Wisdom (ego integrity vs. despair)

## Conditions of flow (Csíkszentmihályi)

- Clear goals (SMART)
- Immediate feedback
- Balance between challenges and skills

## Conditions of flow (Schaffer)

- Knowing what to do
- Knowing how to do it
- Knowing how well you are doing
- High perceived challenges
- High perceived skills
- Freedom from distractions

## Cognitive dissonance (Festinger)

- Attempting to hold two contradictory beliefs results in uncomfortable feeling
- The dissonance is resolved through one of the following:
    1. We change our beliefs to fit the new evidence
    2. We seek out new evidence that confirms our preferred belief
    3. We reduce the importance of the evidence that confirms our unpreferred
    belief

## The four temperaments (Hippocrates)

- Sanguine: active, social, extraverted
- Choleric: independent, goal-oriented, extraverted
- Melancholic: perfectionist, detail-oriented, introverted
- Phlegmatic: easy-going, sympathetic, introverted

## Type A&B personality theory (Friedman & Rosenman)

- Type A personality: ambitious, competitive, hasty, active, impatient, anxious,
  proactive, suffers from a sense of urgency, hates procrastination, can’t relax
- Type B personality: relaxed, reflective, gives himself time to think, patient,
  able to listen to others, with many hobbies and pleasures, not oriented on
  results, lacks clear goals, avoids conflicts

## Myers & Briggs type indicators

- Introverted vs. Extraverted: gains energy alone vs. in social situations
- iNtuitive vs. Sensing: focuses on what might happen vs. what has happened
- Thinking vs. Feeling: focuses on rationality vs. morality
- Perceptive vs. Judging: keeps options open vs. prefers clear decisions

## Givers & takers (Grant)

- Giver: "What can I do for you?"
- Taker: "What can you do for me?"
- Matcher: "I'll do to you what you do to me."

## DOES attributes of highly sensitive persons (Aron)

- Depth of processing
- Overstimulated (over-aroused)
- Emotionally reactive (emphatic)
- Sensitive to subtle stimuli

## Attachment types (Bowlby & Ainsworth)

- Secure
- Anxious: nervous about intimacy
- Avoidant: uncomfortable with intimacy
- Fearful-avoidant / anxious-avoidant: fearful of intimacy

## Sexuality types (Sue Johnson)

- Synchrony sex: balance between pleasure and emotional bonding; secure
  attachment
- Solace sex: used for reassurance and to calm conflicts; anxious attachment
- Sealed off sex: self-focused and preformance oriented; avoidant attachment
- Squabble sex: forceful and passionate; anxious-avoidant attachment

## Habit loop (Fogg & Duhigg)

- Cue (trigger)
- Routine (the action itself)
- Reward
- Craving (anticipation)

## Paradox of Choice (Schwartz)

- Having too much choice results in an inability to make a decision or being
  less satisfied with our decision
- People have the capacity to process 7 'bits' of information at one time
  (Miller)

### Satisficers vs. maximisers (Simon)

- Satisficer (satisfaction + sacrifice): makes choices by establishing a
  threshold of the acceptable levels of attributes, chooses the first option
  passing the threshold, and stops searching
- Maximiser: strives to find the best, seeks to consider all possible options

## Types of power (Zigarmi)

### Illegitimate

- Reward power: using the promise of compensation to make people conform
- Coercive power: using threats and punishment to make people conform
- Referent power: power based on people's desire to identify with the powerful;
  leads to emotional dependence

### Legitimate (justifiable)

- Reciprocity: making people feel obliged to return you a favour
- Equity power: making people feel that you expect compensation for your efforts
- Dependence power: making people feel they are obliged to help you
- Expert power: dependence on your expertise
- Information power: abusing your ability of being persuasive

## Four tendencies (Rubin)

### Expectation types

- Outer expectation: e.g. work deadline, request from a friend
- Inner expectation: e.g. a new year's resolution

### Character types by how they respond to expectations

- Upholder: readily meets outer and inner expectations
- Questioner: does something if it makes sense; turns all expectations into
  inner expectations
- Obliger: readily meets outer expectations, needs outer accountability to meet
  inner expectations
- Rebel: resists all expectations

## Synthetic happiness theory (Gilbert)

- Natural happiness: happiness by getting what we want
- Synthetic happiness: happiness by changing our views of the world so that we
  feel good about a non-favourable situation; a result of unconscious cognitive
  processes
- Natural and synthetic happiness are equivalent
- __Impact bias__: our tendency to overestimate the length and intensity of our
  future hedonic state as a result of an event (e.g. failing an exam, getting a
  promotion)

## AWARE method for overcoming anxiety (Beck)

1. Acknowledge (don't try to escape the feeling)
2. Wait (observe yourself until you're ready to act)
3. Act (deep breathing, PMR, meditation, etc.)
4. Repeat
5. End (remind yourself that it is a temporary state)

## Emotional competencies (Goleman)

- Self-awareness
- Self-regulation
- Motivation
- Empathy
- Socialization

## Appraisal theory of forming emotions

- Stimulus + interpretation + identification + repetition = strong emotion
- Interpretation: your perception of an event based on your personal story
- Identification: the attention you give to the feeling
- Repetition: holding onto the feeling

## IMPROVE methods for regulating emotions (Linehan)

- Imagery (visualising a peaceful setting)
- Meaning in life (purpose)
- Prayer (meditation)
- Relaxation (deep breathing, stretching, yoga, etc.)
- One thing in mind (mindfulness)
- Vacation (break from routine)
- Encouragement (self-motivation)

## Sedona Method of releasing emotions (Dwoskin)

Focus on a feeling and ask the following questions repeatedly:

1. Could I...
    - let this feeling go?
    - allow this feeling to be here?
    - welcome this feeling?
2. Would I?
3. When?

## Phases of anger (Twerski)

1. Anger: the feeling itself
2. Rage: the reaction to anger
3. Resentment: hanging onto the feeling of anger

## Causes of anger (George Patriki)

- Unmet needs — threat to safety
- Grief and loss
- Boundaries being violated by controlling people
- Disappointment & smashed pictures or expectations
- Guilt and shame based identity
- Unforgiveness — bitterness, resentment & revenge
- Vitamin deficiency (e.g. vitamin B3, B6, zinc)
- Substance abuse

### Guilt vs shame

- Guilt: I did bad (behaviour)
- Shame: I am bad (identity)

## A-B-C-D model of anger management (Ellis)

1. Activating situation: that triggers anger
2. Belief system: how the situation is (mis)interpreted
3. Consequences: feelings & physiological effects
4. Dispute: examining behaviour and reinterpreting the situation

## The five stages of grief (Kübler-Ross)

1. Denial
2. Anger
3. Bargaining
4. Depression
5. Acceptance

## Acceptance and commitment therapy (Hayes)

### FEAR (negative approach)

- Fusion with your thoughts
- Evaluation of experience
- Avoidance of your experience
- Reason-giving for your behavior

### ACT (positive approach)

- Accept your reactions and be present
- Choose a valued direction
- Take action

## The four shame responses (Rubin & Lyon)

1. Attacking others ("You're toxic")
2. Attacking yourself ("I'm toxic")
3. Denial (dissociation)
4. Withdrawal (isolation)

## Manipulation

- Persuation is manipulative when one tries to get the other to adopt what they
  themselves regard as a false belief (Robert Noggle)
- __Sarcasm__: belittling someone by taking what they said out of context and
  exaggerating it to the point where it appears stupid or inane
- __Invalidation__: deliberately not acknowledging what someone says
- __Blame shifting__: responding to an accusation with accusation
- __Projection__: accusing someone of things that the manipulator actually does
- __Word salad__: jumping from topic to topic to distract from the original
  topic
- __Hurt & rescue__: hurting someone and immediately coming up with a solution
  that makes the manipulator look superior
- __Gaslighting__: encouraging someone to doubt their own judgment and to rely
  on the manipulator’s advice instead
- __Guilt trip__: making someone feel guilty about failing to do what the
  manipulator wants them to do
- __Charm offensive__: inducing someone to care so much about the manipulator’s
  approval that they will do as the manipulator wishes
- __Impostor syndrome__: when someone doubts their accomplishments, and has a
  persistent internalised fear of being exposed as a fraud (vs. __Dunning–Kruger
  effect__: when someone who is incompetent overestimates their accomplishments)

## Responses to manipulative abuse (Michele Lee Nieves)

- Response to blame shifting & projection:
    - "Thank you for letting me know how you feel."
    - "I just wanted you to know how I felt."
    - "I am sorry you feel that way."
- Response to word salad:
    - "I hear you."
    - "Thank you for expressing your viewpoint."
- Response to gaslighting & guilt trip:
    - "I choose to see things differently."

## Dependency disorders

- __Projection__: attributing feelings someone has about another person to that
  person, and imagining that those feelings are projected back at them
- __Transference__: redirecting feelings someone has about a person to a
  different person based on superficial similarities 
- __Enmeshment__: a relationship in which personal boundaries are so unclear
  that one owns the other’s emotions 
- __Codependence__: a relationship in which one obsessively needs to take care
  of the other
- __Dependent Personality Disorder (DPD)__: an excessive need to be taken care
  of and validated by others
- __Borderline Personality Disorder (BPD)__: a pattern of swinging moods,
  self-image and behaviour; e.g. impulsiveness (rage and self-harm), a fear of
  abandonment, swinging between extreme closeness and extreme dislike

## Nonviolent communication (Rosenberg)

### Violent communication

1. Observation: what's happening
2. Interpretation: what I _think_ about _you_
3. Reaction
    - Benign: analysis, diagnosis, advice
    - Hostile: judgement, demand, criticism, expectation

### Nonviolent communication

1. Observation: what's happening
2. Feelings: Identify _my_ emotional response
3. Needs: Figure out the unmet need that is driving that feeling
4. Request: Explicitly _ask for_ (don't _demand_) what I need

## The four 'D's of disconnection (Rosenberg)

- Diagnosis (i.e. judgements, criticism, comparison)
- Denial of responsibility
- Demands
- Deserve-oriented language

## Meaningful appreciation (Rosenberg)

Appreciation only feels genuine if it's based on:

- Actions that have contributed to our well-being
- Needs of ours that have been fulfilled
- Pleasureful feelings caused by the fulfilment of those needs

## The "four horsemen of relationship apocalypse" (Gottman)

- Criticising your partner's character ("You're so stupid" vs. "That thing you
  did was stupid")
- Blame shifting ("I wouldn't have done that if you weren't late all the time")
- Contempt (making your partner feel inferior)
- Stonewalling (withdrawing from an argument and ignoring your partner)

## The main issues couples argue about (Linda Blair)

- A perceived imbalance of power or lack of reciprocity
- Lack or loss of trust
- Lack or loss of respect
- Lack of understanding about differing needs for space and independence

## Reflective listening (Rogers)

### DOs

- Show positive body language
- Summarise what your partner said using their own words ("So what you're saying
  is…", "So in other words…")
- Confirm that your partner is understood by reflecting their *feelings* back
- Assure your partner that they're not alone

### DON'Ts

- Beg the question (e.g. "How are you" over "Are you OK")
- Give advice, give your personal interpretation, or offer a solution
- Bring up your own similar experiences
- Analyse or trivialise the problem
- Lapse into silence, letting the other do a monologue
- Repeat the same phrases
- Pretend understanding when you get lost. If you find it difficult to focus,
  repeat the other's words in your head
- Say or do anything that distracts from the topic

### Examples

- Statement: "I don't like the coat you're wearing."  Reply: "You really dislike
  this coat."
- Statement: "I hated living in that house."  Reply: "It was pretty bad there
  for you."
- Statement: "Why is it always me?"  Reply: "It doesn't sound fair, does it?"

## Assertive communication (Gordon)

- Use a firm but polite tone
- Say 'no' when you mean 'no'
- Be factual and accurate; don't judge or generalise (e.g. avoid "you always…")
- Use _I-statements_ instead of _you-statements_ (e.g. "I disagree" instead of
  "you're wrong")
- Comment on the effects of your partner's behaviour on you: "When you [do
  something] then [something happens], and I [feel something]."

## DESC script for assertiveness

- Describe what you experience in neutral language
- Explain its effect on you
- Show that you understand how your partner feels
- Communicate your proposed solution

## Dealing with angry people

- Respond calmly; fighting fuels anger. If you can't, flee
- Ask questions empathically to find the cause. Use reflective listening
- Understand that it isn't your fault, in order to distance yourself
  emotionally; it helps you keep your calm (Blechert)
- If it's your fault then apologise, don't be defensive; defensiveness fuels
  anger
- Try to distract the person instead of ruminating on the problem; rumination
  fuels anger. Try to turn anger into laughter
- There is direct correlation between perceived powerlessness and ensuing
  aggression

## Funnel technique of questioning

- Redirect attention gradually from generic to specific
    1. Open prompts ("Tell me more about X")
    2. Focusing questions ("How could you clarify X?")
    3. Linking questions ("What did you mean by X?")
    4. Challenging questions ("Have you talked to Y about it?" / "Do you think Y
    did it deliberately?")
    5. Closing questions ("Is there anything else you'd like to tell about X?")
- Goal: extracting _needs statements_ from blaming comments

## Conflict resolution

1. Calm down (deep breating, walk, bath, contemplation etc.)
2. Share each perspective (no discussion, just listen, take notes)
3. Discuss, negotiate and compromise
- Take a break between each phase, sleep on it if possible

### Resolution styles

- Passive: I lose, you win
- Aggressive: I win, you lose
- Assertive: I win, you win

## Theories of violence (Rai)

- __Disinhibition theory__: everyone has violent impulses they can control most
  of the time; self-control occasionally breaks down because of exhaustion or as
  a response to provocation
- __Rational theory__: violence is a way of achieving goals; the intent is
  gaining power
- __Moral theory__: people are violent because they feel that their violence is
  a moral right or obligation in a certain situation; the intent is regulating
  social relationships according to a moral order

## Persuasion techniques

- __But-you-are-free__: when you make a request, emphasise the other person's
  ability to choose otherwise ("Could you take out the garbage? But it's OK if
  you don't.")
- __Foot-in-the-door__: make a large demand after the other said yes to a small
  one
- __Door-in-the-face__: make a small demand after the other said no to a large
  one
- __That's-not-all__: make a large demand and immediately, before the other has
  a chance to respond, lessen it to a smaller demand
- __Low-ball__: make an initial offer that looks attractive, and only reveal the
  full details after the other said yes
- __Social proof__: when you make a request, refer to others who have already
  said yes
- __Nudge__: the way options are presented affect people's choices, e.g. most
  choose the second cheapest option to avoid looking stingy

## Prejudice

- __Prejudice__: a preconception that cannot be falsified without emotional
  resistance (Allport)
- __Availability heuristic__: our decisions are based on the most readily
  available information, not on accurate information
- __Representativeness heuristic__: we judge two people as similar based on
  superficial similarities
- __Confirmation bias__: we assume that every piece of evidence that confirms
  our belief is a general proof, while the ones that contradict it are fake or
  one-off exceptions to the rule
- __Halo effect__: we take one aspect of someone's personality as a proxy for
  their overall character
- __Pluralistic ignorance__: we assume that even though others' behaviour is
  similar to ours, their behaviour reflects different feelings or beliefs from
  ours
- __Individuation__: when we form close relationship with someone from an
  out-group, we attribute all their positive traits to the individual, and not
  to the group
- __Contact hypothesis__: the more people we contact from an out-group, the more
  we generalise the experience to the entire group
- __Extended contact hypothesis__: knowing that a member of the in-group has
  close relationship with someone from an out-group reduces our prejudice
  towards the entire group 
- __Recategorisation__: shared identity is achieved by redefining the groups
  themselves to be more inclusive (e.g. fans of a sports team)
- __Diffusion of responsibility__: we are less likely to take responsibility for
  action or inaction when others are present, because we assume that they are
  responsible, or already acted (special cases are _social loafing_ and
  _bystander effect_)
- __Aversive racism__: unconscious discrimination by people who otherwise
  maintain an egalitarian self-image, caused by biases that only manifest in
  "edge" situations (e.g. hiring someone from a group of equally qualified
  candidates)
- __Shifting baseline syndrome__: we assume that the circumstances we were born
  into are the standard, and we measure everything relative to these standards
- __Complex contagion theory__: we have to be exposed to a new belief or
  behaviour from multiple sources before we adopt it
- __Groupthink__: occurs when people maintain harmony and conformity inside a
  close-knit group by enforcing uniformity and isolating themselves from outside
  influences (Janis)
- __Selective empathy__: we are only capable of feeling empathy towards
  individuals we are connected to, but not towards a group

## Backfire effect (Gimbel & Harris)

- Providing more evidence to challenge someone's _core beliefs_ makes it less
  likely that they change their mind
- People interpret these arguments the same way as physical threat
- Arguments should be _reframed_ to reflect the worldview of those we'd like to
  convince

## Illusion of explanatory depth (Sloman & Fernbach)

- We rate our depth of knowledge higher than it actually is because we think we
  know how something is even if we are unable to explain it
- We have difficulties making a difference between our own knowledge and the
  people's we cooperate with
- __Dunning-Kruger effect__: the more shallow our knowledge about something, the
  more likely we overestimate our competence in it

## Types of isolation (Holt-Lunstad & Smith)

- __Social isolation__: having few social connections or interactions; doesn't
  imply low perceived quality of life
- __Loneliness__: the subjective perception of isolation, i.e. the discrepancy
  between the desired and actual quantity and quality of social connections

## Nine causes of depression & anxiety (Hari)

1. Disconnection from meaningful work
2. Disconnection from other people
3. Disconnection from meaningful values
4. Disconnection from childhood trauma
5. Disconnection from the natural world
6. Disconnection from a hopeful or secure future
7. Lack of status and respect
8. Genes
9. Neuroplasticity: thoughts, words and deeds (behaviour) that actually rewire
   our brain (create new connections)

## Austrian schools of psychotherapy

- __Psychoanalysis__ (Freud): life's driving force is will to pleasure
- __Individual psychology__ (Adler): life's driving force is will to power (or:
  bringing inferiorities to an end)
- __Logotherapy__ (Frankl): life's driving force is will to meaning

## Logotherapy (Frankl)

- Search for meaning is the primary motivation in life
- Mental health is based on a tension between what someone has achieved and what
  someone still ought to accomplish
- Meaning is
    - given by a cause to serve or a person to love
    - to be found in the world, not through introspection
    - specific to a person's life at a given moment
- Meaning can be discovered by
    - creating a work or doing a deed
    - experiencing something or encountering someone
    - finding purpose in unavoidable suffering
    - prerequisite: redirecting attention away from the self

### Types of neurosis in logotherapy

- Hyper-intention: an excessive desire for some event to happen, which makes
  that event less likely to happen
- Hyper-reflection: an excessive attention on the self as an attempt to avoid
  neurosis, which makes the percieved neurosis even stronger
- Anticipatory anxiety: the fear of an undesired outcome, which makes that
  outcome more likely to happen

## Self-expansion model (Aron & Aron)

- The primary motivation in life is to self-expand
- Self-expansion can be achieved through close relationships that leads to the
  inclusion of the other's resources (social networks), perspectives and
  identities in the self
- The desirability of a partner is judged upon the potential amount of
  self-expansion possible from the relationship

## Meaning in life (Susan Wolf)

- Meaning in life comes from being absorbed in activities that we find
  _objectively_ good
- Meaning in life arises when subjective attraction meets objective
  attractiveness

## Gratification vs. fulfilment (Sean Stephenson)

- Gratification: pleasure now, pay later
- Fulfilment: effort now, pleasure later
