# Pedagogy Notes

## Mental digestive process of making ideas (Young)

1. Gather information
2. Reflect on it
3. Let the subconscious mind work
4. The idea "magically" appears

## SMART criteria of setting goals (Drucker)

- Specific
- Measurable
- Achievable
- Relevant (i.e. significant for you)
- Time-bound

## 5 Second Rule of productivity (Robbins)

- Have an idea (push moment)
- Act on it within 5 seconds whether you feel like it or not (activation energy)
- If you fail to act within 5 seconds, your mind will talk you out of it

## Implementation intention (Gollwitzer)

- Making a plan makes it more likely that we'll act on reaching a goal
- Sharing our goal with someone who acknowledges it makes it less likely we'll
  act on it (sharing already makes us feel accomplished)

## PQRST method of studying (Atkinson)

1. Preview
2. Question
3. Read (and Reflect)
4. Self-recite
5. Test

## Mindsets (Dweck)

- __Fixed mindset__: we're born with a fixed level of intelligence, the purpose
  of learning is to be smart
- __Growth mindset__: intelligence can be developed with effort, the purpose of
  learning is to grow
- Learners should neither be praised for pure effort nor for being clever but
  for their continuous improvement

## Learning styles (Fleming)

- __Visual__: most easily learn what they see
- __Auditory__: most easily learn what they hear
- __Kinesthetic__: most easily learn while they move

## Learning styles (Honey & Mumford)

- __Activist__: the purpose of learning is having an experience
- __Reflector__: the purpose of learning is reflecting on an observation
- __Theorist__: the purpose of learning is drawing a conclusion
- __Pragmatist__: the purpose of learning is putting theory into practice

## Bloom’s taxonomy

Levels of understanding are:

1. Knowledge
2. Comprehension
3. Application
4. Analysis
5. Synthesis
6. Evaluation

## Classroom engagement techniques

- __Cold calling__: asking a random student to answer a question
- __Call and response__: asking all students to answer a question in unison
- __Everybody writes__: asking a question and leaving time for students to
  prepare the answer
- __Targeted questioning__: addressing questions of various difficulty levels to
  specific students based on their abilities
- __Everyone responds__: asking all students to react to a question, e.g.
  nodding or shaking their heads
- __Popcorning__: a student gives an answer to a question and asks another
  student to give their answer

## Social rules of teaching (Hacker School Manual)

- _No feigning surprise_: don't act surprised if a student doesn't know
  something that they "should"
- _No well-actuallys_: don't correct students if they give an answer that is
  almost but not entirely correct
- _No back-seat driving_: don't half-participate in a group's work; only offer
  your advice if you can fully engage
- _No subtle -isms_: avoid slips of the tongue that may make people feel
  unwelcome (e.g. "it's easy")

## Constructivist teaching

- __Development__: spontaneous process initiated by the learner, the result of
  independent thinking (Piaget)
- __Zone of Proximal Development__: the problems a learner can solve unaided are
  expanded by problems the learner can solve with a teacher's help (Vygotsky)

## Problem-based learning

- __Socratic method__: the mediator asks questions about a topic that help
  learners distinguish what they understand from what they don't
- __Enquiry-based learning__: students pose questions and define problems about
  a topic in a mediated group discussion in order to draw a conclusion
- __Anchored instruction__: students freely explore and define problems around a
  theme called the 'anchor'

## Cooperative learning

- __Reciprocal teaching__: students alternate roles as learners (posing
  questions) and teachers (collecting evidence and answering the questions)
- __Jigsaw__: students become experts on one part of a group project and teach
  it to the others
- __Structured controversies__: students work together to research a topic

## Flipped classroom teaching method (Salman Khan)

- Pupils watch lectures and read at home
- In class they ask questions and practice 
- Teachers are not instructors but coaches/mentors

## Feynman technique of studying

1. Study a subject
2. Imagine teaching it to someone, as if in a class
3. Do the same but using as simple language as possible

## Multiple intelligences (Gardner)

- Visual – spatial
- Musical – rhythmic
- Verbal – linguistic
- Logical – mathematical
- Bodily – kinesthetic
- Interpersonal
- Intrapersonal
- (Naturalistic)

## Drive theory of job satisfaction (Pink)

- __Autonomy__: the urge to be self-directed
- __Mastery__: the urge to get better skills
- __Purpose__: the urge to do something that has meaning

## Self-determination theory of motivation (Deci & Ryan)

### Innate needs (ARC)

- __Autonomy__: the perception that we have choices and we are the source of our
  actions
- __Competence__: the need to develop and demonstrate our skills
- __Relatedness__: the need to care and being cared about

### Types of motivation

- __Intrinsic__: driven by innate needs, optimal
- __Extrinsic__: driven by rewards or punishment, suboptimal

## Motivational outlooks (Facer & Fowler)

### Suboptimal

- __Disinteresred__: finding no value in an activity
- __External__: participating in an activity for rewards or to avoid punishment
- __Imposed__: participating in an activity because of peer pressure

### Optimal

- __Aligned__: participating in an activity because it aligns with one's values
- __Integrated__: participating in an activity because it is linked to one's
  purpose (meaning)
- __Inherent__: participating in an activity because of the enjoyment (flow)
- People with integrated and aligned motivational outlooks have high quality
  *self-regulation*

## Transfer of learning

- The process of applying something one has learnt to a new situation
- __Near transfer__: to a similar situation or context
- __Far transfer__: to a completely different situation or context

## Types of fake knowledge

- __Idle knowledge__: that you can recall but unable to apply (other than in
  exams and quizzes)
- __Ritual knowledge__: that you can apply but no longer remember the meaning of
- __Double knowledge__: that you can apply in safe situations but ignore when
  solving genuine problems
- __Foreign knowledge__: that is accumulated about a topic without relating to
  it

## Heuristic techniques

- Trial and error
- Rule of thumb
- Educated guess
- Intuition
- Common sense
- Profiling
