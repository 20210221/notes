# Programming Glossary

<!-- toc -->

- [Programming Languages](#programming-languages)
- [Programming Basics](#programming-basics)
- [Code Transformation](#code-transformation)
- [Data Types & Encodings](#data-types--encodings)
- [Data Structures](#data-structures)
- [Algorithms](#algorithms)
- [Automata](#automata)
- [Approximate Timings](#approximate-timings)
- [Object-oriented Programming](#object-oriented-programming)
- [Functional Programming](#functional-programming)
- [Operating Systems](#operating-systems)
- [Concurrency](#concurrency)
- [Program Design](#program-design)
- [System Design](#system-design)
- [UML](#uml)
- [Design Patterns](#design-patterns)
- [Testing](#testing)
- [Version Control](#version-control)
- [Release Engineering](#release-engineering)
- [Networks](#networks)
- [Cryptography](#cryptography)
- [Distributed Systems](#distributed-systems)
- [Databases](#databases)
- [Relational Databases](#relational-databases)
- [Graph Databases](#graph-databases)
- [Web Development](#web-development)
- [Web Security](#web-security)
- [HTTP Methods](#http-methods)
- [XML](#xml)
- [Regular Expressions](#regular-expressions)
- [gRPC](#grpc)
- [React](#react)
- [Redux](#redux)
- [Docker](#docker)
- [Kubernetes](#kubernetes)
- [Blockchains & Bitcoin](#blockchains--bitcoin)
- [Ethereum](#ethereum)
- [Tendermint](#tendermint)
- [GStreamer](#gstreamer)

<!-- tocstop -->

## Programming Languages

- __imperative language__: its commands prescribe how the program should work,
  i.e. statements that change the program state (e.g. the C family)
- __declarative language__: its commands describe what the program should do,
  i.e. the logic of a computation without describing the control flow (e.g.
  SQL)
- __functional language__: a subset of declarative languages in which every
  function is a _pure function_, i.e. association between inputs and outputs
  without internal state and any side-effects (e.g. Haskell, the Lisp family)
- __general purpose language__: designed for a wide variety of applications
  (e.g.  C, Java, Python)
- __domain-specific language__: dedicated to and optimised for a particular
  problem or solution technique (e.g. PHP, JavaScript, SQL)
- __typed language__: operations require that all operands are the same data
  type
- __single-type language__: has one data type, usually string (e.g. Bash)
- __statically typed language__: types are predefined and fixed at compile time
  (e.g. C, Java)
- __dynamically typed language__: types are discovered at runtime by the
  interpreter (e.g. Ruby, Python)
- __strongly typed language__: data types are always enforced in operations
  (e.g.  C, Java, Ruby, Python)
- __weakly typed language__: data types are implicitly converted in operations
  (e.g. JavaScript, Visual Basic)
- __object-oriented language__: has the concept of _objects_, i.e. allows
  encapsulating data and procedures into a mutable data type
- __strictly / pure object-oriented language__: in which no function and
  variable can exist outside objects, every data type is object type and all
  operations are invocations of object methods (e.g. Java, C#)
- __homoiconic language__: in which the program code is represented as data
  (e.g. in a Lisp language a program is a nested list)
- __type safety__: feature of strongly typed languages; the compiler or
  interpreter doesn't allow intermixing instances of different data types in the
  same operation
- __type introspection__: feature of dynamically typed languages; the ability of
  an interpreter to to determine the type of a variable at runtime
- __reflection__: the ability of an interpreter to modify the signature of an
  object at runtime
- __meta-programming__: writing programs that generate or modify other programs
- __variable hoisting__: a feature of the JavaScript interpreter that allows
  using a variable before it's declared; the interpreter moves all declarations
  to the top of the scope
- __REPL (read-eval-print loop) / language shell__: an interactive programming
  interface that takes single expressions, evaluates them, and presents the
  results to the user
- __evaluation strategy__: programing language design approach around how
  expressions are evaluated and how data is shared with subroutines

## Programming Basics

- __literal__: the representation of a fixed value in the source code (e.g.
  `1`, `"a"`)
- __statement__: any block of code that alters the program state
- __expression__ / __form__: a block of code that evaluates to a value
- __eager evaluation__: evaluation strategy whereby an expression is evaluated
  as soon as it is defined
- __lazy evaluation__: evaluation strategy whereby the evaluation of an
  expression is delayed until its value is first accessed
- __variable__: a memory location identified by a name and associated with an
  encoding (data type)
- __parameter__: a variable that stores an input value of a subroutine
- __argument__: a value passed as an input into a subroutine or process
- __subroutine__: a named unit of instructions, facilitating reuse of code
- __function__: subroutine that has explicit output(s)
- __procedure__: subroutine that doesn't have an explicit output but changes the
  program state
- __declaration__: specifies the identifier, type and parameters (if any) of a
  variable or subroutine
- __signature__: declaration of a subroutine that specifies the identifier,
  the inputs and the outputs but omits the subroutine's body
- __definition__: declaration that assigns a value to a variable or gives the
  body of a subroutine
- __header__: source code that contains declarations and compiler directives
  only
- __variadic function__: a function that accepts any number of arguments
- __macro__: a named sequence of instructions; macro calls are replaced with the
  contents of the macro in the source code before compilation
- __directive__: an instruction for the compiler on how the code should be built
- __callback__: an executable block of code that is passed as an input parameter
  to other blocks of code
- __closure / lexical binding__: the context that includes all variables that a
  block of code can access
- __collection__: an organisation of instances of the same data type; e.g.
  different kinds of lists, sets
- __generics__: functions or classes that receive data types as input or
  initialisation parameters, this way a single function or class can handle a
  variety data types; mostly used to implement collections
- __namespace__: a section of the program code in which identifiers have to be
  unique
- __fully qualified name (FQN)__: unambiguous identifier of a subroutine or
  variable that includes the namespace and all names in the hierarchy within the
  namespace
- __garbage collector__: automatic memory manager of an interpreter, frees the
  dynamic memory allocations that are referenced by variables that are _out of
  scope_ (cannot be accessed in the future)
- __remote procedure call (RPC)__: invocation of a function that is executed on
  another computer via message passing, but called as if it was a local
  function; implementation details are hidden
- __exception__: an error that propagates through the _call stack_ of the
  function in which it occurred; it may be _caught_ (handled) at any level of
  the call stack
- __checked exception__: an exception that is anticipated as it signals a
  recoverable condition, i.e. it should be caught at some level
- __unchecked / runtime exception__: a programming error that should not be
  caught
- __cache__: temporary storage that speeds up fetching data from a data store;
  if the requested piece of data is in the cache (_cache hit_) it is returned
  from the cache, otherwise (_cache miss_) it is copied from the data store into
  the cache
- __cache invalidation__: the process of removing pieces of data from the cache
  that become _stale_ (out of date)
- __parsing__: transforming text into a data structure of a programming language
- __memoization__: storing the results of lengthy function calls, and returning
  the cached result instead of re-executing the function if the function is
  called again with the same inputs
- __serialisation / marshalling__: converting an instance of a data structure
  into a format that can be distributed or stored persistently and an exact copy
  can be restored
- __off-by-one error__: programming error in which a loop iterates one time too
  many or too few

## Code Transformation

- __building__: creating a deliverable software from source code
- __compiling__: transforming the source code into machine code with a
  _compiler_
- __translating__: transforming the source code in a high level language into
  another high level language
- __interpreting__: transforming the source code into _byte code_ and executing
  the code, without compiling into machine code, on a virtual machine with an
  _interpreter_
- __abstract syntax tree (AST)__: a tree representation of the source code,
  built by the source code parser during compilation
- __ahead-of-time (AOT) compilation__: compiling the byte code together with the
  virtual machine into machine code, so that it can run _natively_ (i.e. without
  a virtual machine)
- __just-in-time (JIT) / dynamic compilation__: the interpreter compiles parts
  of the byte code into machine code continuously at runtime
- __packaging__: preparing the software for distribution and deployment
- __byte code__: intermediate code that the interpreter executes,
  platform-independent
- __assembler code__: machine instructions in human readable format,
  platform-dependent
- __object code__: machine instructions of one program module in binary code
  without links to other program modules, platform-dependent
- __machine code__: machine instructions of a complete program in binary code,
  can be executed directly by the computer
- __preprocessing__: 1st stage of compilation; evaluating compiler directives
  and macro expansion
- __compilation proper__: 2nd stage of compilation; transforming the source code
  to assembler code
- __assembly__: 3rd stage of compilation; transforming the assembler code to
  object code
- __linking__: 4th stage of compilation; combining several object code files
  together
- __shared library / shared object__: object code that is used by several
  executables
- __static linking__: linking is performed during compilation; shared libraries
  are included in the binary file
- __dynamic linking__: linking is performed runtime; shared libraries are linked
  by the OS when the program is executed
- __binding__: looking up the function code for a function call
- __early / static binding__: binding is performed during compilation; the
  address is fixed
- __late / dynamic binding__: binding is performed runtime when the function is
  called

## Data Types & Encodings

- __persistent data__: that are preserved after the program that created them is
  terminated; or: data of which previous versions are preserved after they are
  modified, keeping a history
- __constant data__: that are assigned at compile time and cannot be changed
- __immutable / read-only data__: that are assigned at creation (definition) and
  cannot be changed
- __primitive type__: predefined in the language as a basic building block
- __compound type__: built by combining primitive types
- __pointer__: data type whose instance is a memory address; the referenced
  value is accessed via explicit indirection called _dereferencing_
- __reference__: data type that provides access to a value at some other memory
  address; the referenced data is accessed via implicit indirection
- __instance variable__: its value is stored on a stack
- __reference variable__: its value is stored on the heap, accessed through a
  reference stored on a stack
- __function pointer / delegate__: pointer or reference to the code of a
  function
- __recursive type__: a compound type that references an instance of itself
- __call by value__: evaluation strategy whereby data is shared with a
  subroutine by copying the value of the variable to the local stack of the
  subroutine, i.e. the subroutine is working with a copy
- __call by reference__: evaluation strategy whereby data is shared with a
  subroutine by copying a reference to the data structure to the local stack of
  the subroutine, i.e. the subroutine is working with a shared instance of the
  data structure
- __structure__ (type): an ordered set of a fixed number of arbitrary elements;
  each _record_ (element) of the structure can be accessed via a named reference
- __enumeration__: an ordered list of constants; each constant can be accessed
  via a named reference
- __Boolean__: data type with two possible values, _true_ and _false_; usually
  encoded as the unsigned integers _1_ and _0_
- __flag__: a Boolean value stored on 1 bit, or: a value of any data type that
  is used to represent _true_ or _false_
- __truthy__ / __falsy__: a value of any type that is converted to Boolean as
  _true_ / _false_
- __predicate__: an expression that evaluates to a Boolean value
- __null__ / __nil__: an alias for the 0 memory address that may be assigned to
  pointers that don't reference any valid memory location
- __MSB (most significant byte/bit)__: the byte or bit in a binary number
  representation that has the highest value
- __endianness__: the ordering of bytes in a multiple-byte-long binary
  representation of a number; a _big-endian_ ordering has the most significant
  byte first, a _little-endian_ ordering has it last
- __signed integer__: _[-(2<sup>length - 1</sup>), 2<sup>length - 1</sup> - 1]_
  range; the 1st bit is the _sign bit_
- __unsigned integer__: _[0, 2<sup>length</sup> - 1]_ range
- __floating point__: decimal fraction encoding as _value = ± significand ×
  10<sup>-exponent</sup>_
- __single precision__: 32-bit floating point encoding; 1 sign bit + 8 exponent
  bits + 23 significand bits
- __double precision__: 64-bit floating point encoding; 1 sign bit + 11 exponent
  bits + 52 significand bits
- __ASCII__ (American Standard Code for Information Interchange): fixed-length
  character encoding standard using 7 bits of a byte (the first bit is always 0)
- __Unicode__: a group of character encoding standards in which each
  character is represented by a _code point_ as `U+XXXX` where _XXXX_ is a
  4-byte-long hexadecimal number
- __UTF-8__: variable-length Unicode standard; the length of the encoded
  character is indicated by the prefix of the first byte (`0` for 1 byte, `110`
  for 2 bytes, `1110` for 3 bytes etc.) and subsequent bytes start with the `10`
  prefix; backward compatible with ASCII

## Data Structures

- __bit field / bitmap__: an integer in which each binary digit is used as a
  flag
- __sentinel__: a special value in a data structure that is used to signal the
  end of the structure (e.g. null pointer in a linked list, `\0` character in a
  string, `EOF` in a file)
- __array__: an ordered list of elements identified by zero-based indexing
- __vector__ / __list__: a variable-length array
- __n-dimensional / nested array__: composed of 1-dimensional arrays; each
  element of every array up to level _n-1_ is a reference to another array
- __jagged array__: a 2-dimensional array in which each element is a vector
  (has variable length)
- __string__: a list of characters that is implemented as a primitive type in
  some languages and a compound type in others
- __queue / buffer__: linear FIFO (first-in-first-out) array with three
  operations: _put_ (add), _get_ (remove) and an optional _peek_ that returns
  the element from the front of the queue without removing it
- __priority queue__: queue in which each element has a priority, and the one
  with the highest (or lowest) priority is removed first
- __stack__ (structure): linear LIFO (last-in-first-out) array with 2
  operations: _push_ (add) and _pop_ (remove)
- __hash function__: a function that converts data of arbitrary size to data of
  fixed size
- __perfect hash funciton__: a hash function that converts each input to a
  different unique hash value
- __hash table / map / associative array / dictionary__: an array that uses a
  hash function to link keys to values; if the hash function is perfect, values
  can be found without searching; in practical implementations the hash function
  returns an index that redirects to a smaller array called a _bucket_ in which
  the value can be searched for
- __dispatch table__: a hash table that stores references to subroutines
- __linked list__: a group of nodes that together represent a sequence; each
  node has a link to the following node
- __doubly-linked list__: a linked list in which each node has links to both
  directions
- __set__: a structure that stores values without a particular order and has no
  repeated values
- __tree__: an acyclic graph in which each node has at most one parent node
- __binary tree__: a tree in which each node has at most two children
- __binary search tree__: a binary tree in which the value of any node in each
  node's left subtree is less than the node's value, and the value of any node
  in the right subtree is greater than or equal to the node's value
- __balanced binary tree__: a binary search tree that is completely filled on
  all levels except possibly the lowest, i.e. the difference in the height of
  any of the left and the right subtrees cannot be greater than one; insertion
  might require swapping nodes but search is faster
- __heap__ (structure): a balanced binary tree in which each node's value is
  either less than or equal to the value of any of its children (_min-heap_), or
  greater than or equal to the value of any of its children (_max-heap_), and
  all leaves are in the leftmost position; implements priority queue
- __trie (retrieval tree) / prefix tree__: a search tree used to implement
  hash tables in which the value of any descendant of a node has a common prefix
  with the value of the node
- __radix trie__: a prefix tree in which each node that is an only child is
  merged with its parent
- __Merkle trie__: a tree in which the hash of any node is calculated from the
  hashes of its children, i.e. if any node of the tree is modified, the hash of
  the root also changes
- __Merkle proof__: the list of hashes along a Merkle subtree that are required
  to compute all hashes between the root and a modified node, used to verify
  the validity of the tree when a node is added or modified
- __Bloom filter__: a dataset from which it is very efficient to query whether a
  value is its member or not, with the possibility of false positives as a
  trade-off; a new element is hashed by multiple different hash functions, each
  one setting a flag in a bitmap; a value is not an element of the set if any of
  the flags the hashes point to has not been set

## Algorithms

- __polynomial time__: an upper bound for the running time of an algorithm as a
  polynomial function of the input size
- __P (polynomial) complexity class__: the group of decision problems that can
  be solved in polynomial time
- __NP (nondeterministic polynomial) complexity class__: the group of decision
  problems that cannot be solved, but the correctness of their solution can be
  tested in polynomial time, i.e. a solution can be guessed
- __NP-completeness__: a decision problem is _NP-complete_ if it is in NP and
  all other problems in NP are at most as difficult to solve
- __Boolean satisfiability problem (SAT)__: determining whether a boolean
  formula can be _satisfied_, i.e. there is an assignment of variables where the
  formula evaluates to true; NP-complete
- __heuristic__: a solution that is not optimal but sufficient for solving a
  certain problem
- __greedy algorithm__: that chooses the local best option at every step, not
  taking into account any future decision (e.g. Dijkstra's, Kruskal's)
- __recursion__: solution method that splits a problem into a _base case_ that
  produces a simple answer _x<sub>0</sub>_ and a _recursive step_ that reduces
  the problem towards the base case as _x<sub>n</sub> = f(x<sub>n-1</sub>)_
- __divide and conquer__: solution method that splits a problem into
  _independent subproblems_ (usually by partitioning the inputs) and solves the
  subproblems recursively (on groups of partitions) until the results can be
  combined, which yields the solution to the original problem (e.g. quicksort,
  merge sort)
- __dynamic programming__: solution method that splits a problem into
  _overlapping subproblems_ and stores the results of the subproblems, thus they
  can be reused; (1) top-down approach: subproblems are solved recursively as in
  divide and conquer but the solutions of subproblems are memoized, (2)
  bottom-up approach: a table of the solutions of subproblems is built up where
  the _Nth_ element is calculated using the previously calculated _N-1_ elements
- __traversal__: accessing elements of a data structure
- __data munging__: changing data in a destructive (irreversible) way
- __space-time tradeoff__: the scenario that an algorithm can only be made
  faster by sacrificing memory usage and vice versa
- __commutativity__: a composition of operations is _commutative_ if its result
  doesn't depend on the execution order, i.e. _f(g(x)) = g(f(x))_
- __idempotence__: an operation is _idempotent_ if applying the same operation
  on a dataset multiple times only changes the data once, i.e. _f(x) = f(f(x))_
- __invariance__: a property is an _invariant_ of an operation if it is true
  before and remains true after the execution of the operation
- __asymptotic notation__: describes how the relative running time of an
  algorithm of a computation changes as the input size increases
- __*f(n)*__ is the number of instructions executed to generate the output of an
  algorithm for every input size _n_; to find _f(n)_, count every expression
  (assignment, look-up / indirection, comparison, arithmetic) as one
  instruction; do not count control statements
- __big-O notation__: provides an asymptotic upper bound for _f(n)_ i.e. the
  worst case; _f(n) = O(g(n))_ if _f(n) ≤ c·g(n)_ for every _n > n<sub>0</sub>_
- __big-Omega notation__: provides an asymptotic lower bound for _f(n)_ i.e. the
  best case; _f(n) = Ω(g(n))_ if _f(n) ≥ c·g(n)_ for every _n > n<sub>0</sub>_
- __big-Theta notation__: provides an asymptotic tight bound for _f(n)_ i.e. the
  average case; _f(n) = Θ(g(n))_ if _c<sub>1</sub>·g(n) ≤ f(n) ≤
  c<sub>2</sub>·g(n)_ for every _n > n<sub>0</sub>_
- __approximations__: for large enough _n_
    - ignore constant factors because they are too system-dependent
    - ignore lower-order terms because they become irrelevant
    - _O(1) < O(log(n)) < O(n) < O(n·log(n)) < O(n<sup>c</sup>) <
      O(c<sup>n</sup>) < O(n!)_
    - _O(n!) ≅ O(n<sup>n</sup>)_
    - _O(log(n!)) ≅ O(n·log(n))_
- __effectively constant time__: where _f(n) ≅ O(1)_ for large enough _n_
- __arithmetic progression__: _f(n) = 1 + 2 + ... + n = n·(n + 1)/2 =
  O(n<sup>2</sup>)_

## Automata

- __machine__: a mathematical model of a computer
- __automaton__: (plural: automata) an abstract representation of a formal
  language as a machine
- __finite-state machine (FSM)__: a directed graph in which nodes represent
  states of the system; in each state inputs are processed, which triggers
  state transitions represented by arcs (directed edges)
- __guard__: a condition for a state transition in a state machine that must be
  true in order to traverse the transition
- __Mealy machine__: an FSM in which outputs are determined by the current state
  and the current input, i.e. outputs are associated with state transitions
- __Moore machine__: an FSM in which outputs are associated with states
- __Markov chain__: an FSM in which state transitions are not triggered by
  inputs, but occur randomly; a probability is associated with each transition,
  which probability is never conditional on past states (_Markov property_)
- __Petri net__: a directed graph with two types of nodes, _places_ and
  _transitions_, connected by weighted arcs; places may contain _tokens_ that
  represent resources that move to a new place when a transition is _fired_,
  which can only happen if the number of tokens is sufficient for the arc weight
- __Turing machine__: an automaton with an infinite random access memory
  (modeled with a tape) and a program that can instruct it to read a symbol from
  memory, write a symbol to memory, move to a new memory location and terminate
  with a result; a problem is _computable_ if it can be solved by a Turing
  machine
- __Turing completeness__: property of a programming language that ensures that
  it can simulate a Turing machine, i.e. it can be used to solve any problem
  that can be solved by a Turing machine
- __state space__: the set of all possible orderings of transitions in an FSM
- __partial order reduction__: method for reducing the state space of the FSM
  of a concurrent system whereby concurrent transitions that lead to the same
  state are merged

## Approximate Timings

- Fetch from L1 cache: _0.5 ns_
- Execute an instruction: _1 ns_
- Fetch from L2 cache: _5 ns_
- Fetch from main memory: _100 ns_
- Send 1 KB over 1 Gbps Ethernet: _10 μs_
- Fetch from SSD: _100 μs_
- Read 1 MB sequentially from main memory: _250 μs_
- Packet round-trip time within the same datacenter: _500 μs_
- Read 1 MB sequentially from SSD: _1 ms_
- Read from new HDD location (_seek_): _10 ms_
- Read 1 MB sequentially from HDD: _20 ms_
- Packet round-trip time between the US and Europe: _100 ms_

## Object-oriented Programming

- __class__: a data type that defines a data structure and associated processing
  subroutines
- __instantiation__: creating a representation of the class in the memory
- __object__: an instance of a class in the memory, having unique identity
  (properties), state and behaviour
- __attribute__: any data associated with an object
- __method__: a subroutine associated with an object
- __property__: a variable associated with an object
- __member__: a method or property
- __inheritance__: reuse of code of existing classes by creating a hierarchy of
  derived classes
- __single inheritance__: a subclass can inherit from one superclass
- __multiple inheritance__: a subclass can inherit from multiple superclasses
- __superclass / base class / parent__: class from which another class is
  derived
- __subclass / child__: derivative class that inherits some members of the
  superclass
- __ancestor__: any superclass of a class in the inheritance chain
- __descendant__: any subclass of a class in the inheritance chain
- __method overriding__: defining a method in the subclass with the same
  signature but different behaviour from the one in the superclass
- __method overloading__: defining a method with the same name but different
  signature from other methods in the same class
- __polymorphism__: the ability of a programming language to enable using the
  same interface with different data types, through method overloading, method
  overriding, or generics
- __duck typing__: alternative to inheritance; in a collection of objects of
  various types all objects have methods with the same signature, but they don't
  have a common superclass
- __virtual method__ (C++): a method whose behaviour can be overridden within an
  inheriting class by a method that has the same signature
- __pure virtual method / abstract method__: method that has a declaration but
  no definition and is required to be defined in any inheriting class
- __interface__: a class that contains pure virtual methods only and thus cannot
  be instantiated; all methods that are declared in an interface are public and
  must be defined in its descendants
- __abstract class__: a class that contains at least one pure virtual method and
  thus cannot be instantiated but may contain defined methods and properties as
  well
- __instance member__: a method or property associated with an object
- __static member / class member__: a method or property associated with a
  class, resolved at compile time and created when the program starts
- __static initialiser / metaclass__: special method of the class that runs once
  when the class definition is processed by the interpreter; returns the new
  class
- __instance initialiser__: special method of the class that runs once when the
  object is created, before the constructor runs; returns the new object
- __constructor__: special method of the class to set an object's initial state;
  runs once when the object is created, after the initialisers
- __destructor__: special method of the class that runs once when the object
  goes _out of scope_; used in non-garbage collected languages to free memory
- __finaliser__: special method of the class that runs once when the object is
  garbage collected
- __public member__: visible outside the class and inherited by subclasses
- __private member__: visible only in the class it belongs to
- __protected member__: visible only in the class it belongs to but also
  inherited by subclasses
- __final / read-only variable__: can only get a value once, via an initialiser
  or an assignment statement in the constructor
- __final class__: cannot have subclasses
- __final method__: cannot be overridden in subclasses
- __friend function__ (C++): a function that is not the member of a class, but
  as a _friend_ it gets access to its private and protected members
- __wrapper class__: class that extends a primitive type with methods and
  properties
- __boxing__: converting a primitive data type instance to an object of the
  wrapper class
- __unboxing__: converting a wrapper object to a primitive data type instance
- __helper class__: a class that contains static methods only, to group
  procedural code in strictly object oriented languages

## Functional Programming

- __domain__: the set of all possible input combinations of a function
- __range__: the set of all possible output combinations of a function
- __referential transparency__: property of a function that ensures that it
  always gives the same output for the same inputs
- __side effect__: an operation performed by a function that is observable
  outside the function (other than producing outputs)
- __pure function__: a function that is referentially transparent and doesn't
  produce side effects
- __orthogonality__: property of a system that ensures that its operations don't
  have side effects and every action changes just one thing without affecting
  others
- __first-class citizen__: a data type whose instance can be modified runtime,
  assigned to variables, passed as an argument and returned from functions;
  e.g. functions are first-class citizens in JavaScript but not in C
- __first-order function__: that returns data values as output
- __higher-order function__: that returns functions as output
- __anonymous function / lambda__: a function that does not have an identifier
  and is nested to the containing code block
- __prefix / Polish notation__: fixed structure of expressions as _(operator
  operand…)_, e.g. _1 + 2 + 3_ → _+ 1 2 3_
- __arity__: a function is _n-arity_ if the number of its arguments is _n_
- __currying__: converting a multi-arity function into a sequence of 1-arity
  functions, so that the 1st function accepts the 1st argument and returns a
  lambda function that accepts the 2nd argument etc. as _f(x, y, z) =
  g(x)(y)(z)_
- __composition__: substituting a sequence of _composite functions_ with a
  single function as _f<sub>1</sub>(f<sub>2</sub>(f<sub>3</sub>(x))) = g(x)_
- __generator / lazy sequence__: language construct that returns a list whose
  elements are generated one-by-one as they are accessed, instead of generating
  the entire list on creation
- __map__: higher-order function that applies a function to every element of an
  array and returns an array of the results
- __filter__: higher-order function that applies a function to every element of
  an array and returns an array with those elements of the input array that the
  applied function returns a truthy value for
- __list comprehension__: language construct for applying a filter to an array
  using _set-builder notation_, e.g. _S = { x<sup>2</sup> | x ∈ Z, x ≥ 0 }_
- __reduce / fold__: higher-order function that applies a function to every
  element of an array and returns a single value; in the first step the function
  is called with the first two elements of the array or the first element and an
  initial value, then with the result of the previous step and the next element
  of the array
- __reduce-combine / split-apply-combine__: distributed data processing pattern
  whereby elements of a list are split into partitions, a reduce function is
  applied concurrently on each partition, then a combine function is applied on
  pairs of the results recursively until a single return value is produced
- __MapReduce__: distributed data processing pattern whereby a map function is
  applied on a dataset for filtering and transforming data, then a reduce
  function is applied on the results for summarising data, running in parallel
  over shards of a distributed database
- __tail call__: calling a subroutine as the last statement in a procedure
- __tail recursion__: calling a function recursively as the last statement in
  the function
- __tail call optimisation / elimination__: implementation of tail call so that
  it doesn't add a new entry to the call stack, but instead it replaces the call
  stack of the caller; allows implementing iteration with recursion in
  functional languages without stack overflow

## Operating Systems

- __kernel__: the lowest level of the OS that links applications with the
  computer's hardware
- __shell__: user interface of an OS that provides user access to the OS's
  services
- __sandboxing__: running a program in an isolated environment so that a
  malfunctioning program cannot damage the rest of the system
- __supervisor mode / kernel mode__: protection domain of the OS, processes
  running in supervisor mode have direct access to the computer's hardware
- __user mode__: protection domain of the OS, processes running in user mode can
  access the computer's hardware via _system calls_
- __address space__: a range of memory addresses that is available for a program
- __kernel space__: the memory area that only kernel mode processes can access
- __user space__: the memory area that user mode processes can access
- __user land__: the set of applications running in user mode
- __monolithic kernel__: kernel type where all operating system services run in
  supervisor mode; e.g.  Linux
- __microkernel__: kernel type where the basic OS services run in supervisor
  mode, most OS services run in user mode; e.g. QNX
- __hybrid kernel__: kernel type where most OS services run in supervisor mode,
  some OS services run in user mode; e.g. Windows, Darwin
- __process__: an instance of a binary image being executed; each process has
  separate address space and associated security context
- __thread__: the smallest sequence of instructions and related data that the OS
  can handle independently; threads of a process have shared address space; has
  its own virtualised processor, stack and unique program state
- __task__: a process or a thread
- __child process__: process _Q_ is a child of process _P_ if _Q_ has been
  created by _P_; _P_ and _Q_ form a _process group_
- __fork__: OS operation that creates a child process that is an exact copy of
  the parent process
- __daemon / background process__: a process that runs without user interactions
- __zombie__: a process that has terminated, but hasn't been cleared from the
  process table because the kernel is waiting for its parent to read its exit
  status (with the `wait` system call)
- __orphan__: a running child process whose parent process has terminated
- __signal__: an asynchronous notification sent to a process or process group
  that interrupts its execution
- __static memory allocation__: performed at compile time; memory is reserved
  and released by the OS
- __dynamic memory allocation__: performed during runtime as required, depending
  on the inputs; memory is reserved and released by the program
- __stack frame__: an area of the main memory assigned to a subroutine; stores
  the input parameters, the local variables and the return address; statically
  allocated
- __call stack__: a stack data structure that stores the stack frames of the
  subroutines currently executed in a program
- __global frame__: an area of the main memory that stores the global variables
  of a program
- __heap__ (memory): a large pool of the main memory used to fulfil dynamic
  memory allocation requests
- __memory leak__: failure to clean up an allocation on the heap before the
  allocated memory becomes unreachable as all references get _out of scope_
- __shared memory__: an address space that is accessible by multiple threads or
  processes
- __stack overflow__: the error that occurs when the size of the call stack
  exceeds the limit of the address space, i.e. too much memory is used on the
  call stack
- __stack trace__: a log of the contents of the call stack of a function
- __segment__ (memory): an area of the main memory assigned to a process
- __segmentation fault__: the incident that a process is interrupted by the OS
  because it attempts to access memory outside its assigned address space
- __file descriptor__: a reference to an open I/O resource; the kernel keeps a
  reference count to know when to close a descriptor in case multiple processes
  use the same resource
- __file system__: an index that describes the structure and locations of files
  on a storage device; provides an API for file operations (open, close, read,
  write)
- __volume__: a storage container that the OS identifies as a device
- __logical volume__: a single volume that may consist of multiple physical
  devices
- __partition__: a subdivision of a volume with its own _mount point_
- __inode__: a table in a file system partition that stores metadata related to
  files in the partition; a file in a Unix system is an entry with a name and an
  inode number
- __scheduler__: kernel service that distributes hardware resources between
  concurrent processes
- __preemptive multitasking__: the scheduler temporarily interrupts active tasks
  without their permission, in order to reassign their resources to other tasks
- __non-preemptive / cooperative multitasking__: tasks voluntarily free up
  resources periodically or when they are idle
- __POSIX (Portable Operating System Interface for Unix)__: a set of standards
  that define a common command interpreter, common system functions and
  common utility programs to ensure portability between Unix-based systems
- __shebang__: a directive in scripts that tells the shell which interpreter to
  run the script with; starts with `#!`

## Concurrency

- __wall-clock time__: actual "real world" time as opposed to CPU cycles
- __concurrency__: execution of overlapping tasks, either by distributing the
  tasks to separate processors, or via time sharing on the same processor
- __parallelism__: the type of concurrency where concurrent execution occurs at
  the same wall-clock time, on separate processors
- __coroutine__: a subroutine that may be interrupted and resumed using
  cooperative multitasking; provides concurrency (but not parallelism)
- __native thread__: that is managed by the OS kernel
- __green / lightweight thread__: that is scheduled by a runtime environment or
  virtual machine; emulates a native thread
- __concurrency hazard__: a scenario in which concurrent tasks yield unintended
  results due to synchronisation problems
- __race condition__: the scenario that the result of a calculation depends on
  relative ordering of concurrent events, i.e. the result is affected by
  external operations the task doesn't have control over
- __data race__: race condition where two tasks are attempting to write the same
  shared memory location at the same time
- __starvation__: the scenario that a task doesn't get access to a resource it
  needs and therefore cannot progress
- __deadlock__: the scenario that every task is waiting for another task to
  release a locked resource and therefore cannot progress
- __livelock__: the scenario that two communicating tasks repeat the same
  interactions in an infinite loop and therefore cannot progress
- __thread-safety__: implementation of shared resources so that their
  simultaneous access by multiple threads is free of concurrency hazards
- __concurrency primitive__: a programming language construct for synchronising
  concurrent tasks
- __lock / mutual exclusion (mutex)__: concurrency primitive that ensures that
  only one task can access a shared resource or a _critical section_ of the
  program at one time by setting/resetting a flag; if task _A_ acquires the
  lock first, task _B_ waits until _A_ releases the lock
- __barrier__: concurrency primitive that blocks the execution of concurrent
  tasks at the start of a critical section until all of them reach the section
- __semaphore__: a lock with an associated numeric value and two atomic
  operations, _P_ (wait and decrement) and _V_ (increment); the value of a
  semaphore _S_ is the difference between the total number of resources and the
  number of reserved resources, and a task has to wait if _S ≤ 0_
- __monitor__: a lock with an associated _condition variable_ and two
  operations, _wait_ for the condition, and _notify_ the monitor that the
  condition is satisfied, in which case the tasks waiting for the condition
  continue
- __promise / future__: a reference to the not-yet-computed result of an
  operation where the operation is launched in the background on creation;
  doesn't block the caller until the value is dereferenced, in which case the
  caller is waiting for the result to become available
- __delay__: a reference to the not-yet-computed result of a delayed operation
  where the operation doesn't get executed on creation, only when the value is
  first dereferenced, in which case the caller is waiting for the result to
  become available
- __actor__: an isolated object as a building block of a concurrent system,
  communicates with other actors through asynchronous message passing
- __agent__: an isolated object as a building block of a concurrent system,
  receives tasks and returns the task results
- __software transactional memory (STM)__: synchronisation mechanism that
  provides concurrency primitives for creating atomic operations to access
  shared memory, thereby making locks unnecessary
- __compare-and-swap (CAS)__: synchronisation mechanism for updating the value
  of a shared variable; the value of the variable is first compared to the value
  that the caller knows as the latest, to see if it has changed; the value is
  only updated if it hasn't
- __communicating sequential processes (CSP)__: concurrency model in which
  concurrent processes (or coroutines) communicate with each other through
  synchronous message passing via _channels_
- __global interpreter lock (GIL)__: mechanism used by interpreters to
  synchronise tasks so that only one thread can execute at a time, even on
  multiprocessor systems (Python, Ruby)
- __producer-consumer problem__: the scenario that a _producer_ feeds data into
  a _consumer_ through a shared buffer, and the producer tries to put data into
  the buffer when the buffer is full, or the consumer tries to remove data from
  the buffer when the buffer is empty

## Program Design

- __functional model__: a program is modeled as a mapping of inputs to outputs
- __behavioural model__: a program is modeled as a set of behaviours where a
  behaviour is a sequence of states and a state is an assignment of values to
  variables
- __YAGNI (you ain't gonna need it) principle__: do not add new functionality
  unless you are really sure it is needed
- __DRY (don't repeat yourself) principle__: every piece of knowledge must have
  a single representation within the system
- __KISS (keep it simple, stupid) principle__: prefer maintainable code to
  clever solutions
- __API (Application Programming Interface)__: an external program module used
  by another program; its implementation details are hidden, accessed through a
  well-defined interface
- __library__: a collection of related APIs and configuration
- __framework__: a comprehensive API that the entire application is built around
- __cohesion__: measure of how strongly related the elements of a software are
- __functional cohesion__: a program is functionally cohesive if the modules
  that perform a function together are kept together and everything else is kept
  out
- __temporal cohesion__: a program is temporally cohesive if the modules that
  are used during the same phase of execution are grouped together
- __coupling__: measure of the degree how much a program module relies on other
  program modules; two modules are coupled if one has to change behaviour if the
  other one changes behaviour
- __encapsulation__: bundling data structures together with subroutines
  operating on that data
- __information hiding__: restricting access to some of the object's components
- __SoC (separation of concerns)__: separating a program into features that
  overlap as little as possible using encapsulation and information hiding
- __is-a relationship__: the subclass is the specialised form of the superclass
- __part-of relationship__: the containing class is the owner of the member
  class and the member class cannot exist outside the containing class
- __has-a relationship__: the containing class is the owner of the member class
  but the member class can exist outside the containing class
- __entity__: model of a real-world object (→ class)
- __attribute__: a characteristic of an entity (→ property)
- __relationship__: an interaction between entities (→ method)
- __weak entity__: an entity that depends on the existence of another entity
- __cardinality__: the permitted number of instances of an entity in a model
- __one-to-one, one-to-many, many-to-many relationship__: relationship types
  according to the cardinality of the entities
- __ordinality__: the hierarchical order of entities in a model
- __generalisation__: combining objects through inheritance; implements is-a
  relationship
- __composition__: combining objects through containment; implements part-of
  relationship
- __aggregation__: combining objects through containment; implements has-a
  relationship
- __forwarding__: an alternative to inheritance via containment; an object that
  receives a request uses another object, instantiated as its property, to
  handle the request
- __delegation__: a special case of forwarding where the class to forward to is
  selected from multiple classes that implement the same interface
- __SOLID__: principles of object-oriented design: Single responsibility,
  Open/closed principle, Liskov substitution, Interface segregation, Dependency
  inversion
- __single responsibility__: every class should have a single purpose that is
  entirely encapsulated by that class
- __open/closed principle__: software entities should be open for extension but
  closed for modification, i.e. we should be able to modify the behaviour of a
  class without having to modify the users of the class
- __Liskov substitution__: every object in the program should be replaceable
  with instances of their subtypes without altering any user of those objects
- __interface segregation__: many client specific interfaces are better than one
  general purpose interface
- __dependency inversion__ (principle): high-level modules should not depend on
  low-level modules but low-level modules should depend on high-level ones; or:
  abstractions should not depend on details but details should depend on
  abstractions (see also: pattern)
- __test double__: a function or data structure that stands in for a real one
  for testing, to isolate the code under test
- __dummy__: an empty test double for filling parameter lists
- __stub__: a test double with _canned_ (hard-coded) fake data
- __mock__: a stub that includes logic to verify its user
- __fake__: a test double that is a working implementation, but not suitable for
  production (e.g. an in-memory DB instead of a persistent DB)
- __shim__: a thin application layer that changes arguments, or redirects
  requests in order to resolve compatibility issues between two other layers
- __boilerplate__: a block of code that has to be included in many places of the
  source code, e.g. inclusion of system libraries, class skeletons

## System Design

- __system analysis__: identifying the problem that we're trying to solve and
  assessing any proposed solution in terms of whether or not it solves the
  problem
- __system design__: outlining a solution to a problem; its objective is to
  solve the problem and not to build features, as there is no guarantee that a
  sum of features solves any problem or doesn't introduce new problems
- __design tradeoff__: considering at least two possible solutions and assessing
  the attributes and deficits of each
- __essential complexity__: that is inherent in the nature of a system
- __incidental / accidental complexity__: that is self-imposed and avoidable
  with good design
- __protocol__: a standardised hardware or software specification
- __reference implementation__: the first implementation of a protocol that
  other implementations are derived from or modeled after
- __federation__: establishing rules on the interoperability of two systems
- __requirement__: description of a feature that the system must implement
- __test-driven development (TDD)__: software development approach in which
  tests that cover a requirement are written first, then the minimal amount of
  code is written that is sufficient to pass the tests
- __behaviour-driven development (BDD)__: software specification method with
  narratives decomposed into scenarios
- __use case__: description of interactions between the system and its users
  (_actors_)
- __user story__: description of a requirement in BDD using the template "As a
  [role] I want to [goal] so that [reason]."
- __scenario__: an execution path that satisfies a user story in BDD using the
  template "Given [conditions], when [action] then [expected result]."
- __functional requirement__: specific behaviour of the system, i.e. what a
  system is supposed to do
- __non-functional requirement__: some criteria to judge the operation of the
  system with, i.e. how a system is supposed to be; e.g. speed, security,
  availability, scalability
- __wireframe__: basic design plan for a user interface that shows how
  information will be arranged
- __scaffolding__: generating code automatically based on a specification for
  rapid software development
- __three amigos__: specification approach in which each new feature is examined
  from 3 perspectives: business, development and testing, in a collaboration of
  (at least) 3 people, each one being a specialist of one of the 3 areas
- __SWOT analysis__: planning technique that examines new features / ideas from
  4 perspectives: strengths, weaknesses, opportunities and threats
- __MVP (minimum viable product)__: an early version of a new product with a
  small number of features just enough to receive feedback from early adopters
- __tech stack__: the set of programming languages, frameworks, tools and APIs
  that are used to build an application
- __feature creep__: adding new features to a product for the sake of expanding
  it and not because they add real value
- __bloat__: a decrease in a software's performance and/or usability after
  adding new features
- __footgun__: a feature that highly increases complexity (therefore results in
  the developers shooting themselves in the foot)
- __bikeshedding (Parkinson's law of triviality)__: the tendency of teams to
  devote significant amounts of time to insignificant details (that are easy to
  grasp) in the expense of crucial matters (that are complex)

## UML

- __entity-relationship (ER) diagram__: visualisation of entities as rectangles,
  attributes as ovals and relationships as diamond shapes
- __class diagram__: visualisation of the system's classes with their properties
  and methods and their relationships
- __flow chart__: graph that represents the control flow of an algorithm
- __activity diagram__: high level flow chart representation of the system's
  behaviour
- __package diagram__: grouping of classes into packages to identify
  dependencies
- __data flow diagram__: the messages between the system's objects in space
  (i.e.  the interfaces of a system)
- __sequence diagram__: the message flow between the system's objects in time
- __state diagram__: graph of a finite-state machine in which nodes represent
  states and directed edges represent state transitions
- __statechart__: state diagram of a hierarchy of state machines where some
  states (called _compound states_) have _substates_ that implement lower level
  state machines; states with no substates are called _atomic states_

## Design Patterns

- __look before you leap (LBYL)__: coding style that encourages checking
  preconditions before making calls; preferred in C
- __easier to ask forgiveness than permission (EAFP)__: coding style that
  assumes that the preconditions are satisfied when making calls, and catches
  exceptions if the preconditions prove to be false; preferred in Java & Python
- __design pattern__: general language-independent reusable solution to a common
  problem
- __architectural pattern__: design pattern that deals with the structure of a
  system
- __creational pattern__: design pattern that deals with object creation
- __structural pattern__: design pattern that deals with relationships and
  interfaces between objects
- __behavioural pattern__: design pattern that deals with communication between
  objects
- __concurrency pattern__: design pattern that deals with synchronisation
  between threads or processes
- __client-server model__: architectural pattern in which _server_ nodes provide
  services to _client_ nodes via message-passing; clients send _requests_ to
  access the servers' resources and receive subsequent _responses_
- __singleton__: creational pattern that restricts the instantiation of a class
  to one object and allows global access to that single object via a static
  method
- __eager singleton initialisation__: initialisation method whereby the
  singleton instance is created when the class is loaded
- __lazy singleton initialisation__: initialisation method whereby the singleton
  instance is created the first time it is accessed
- __inversion of control / dependency inversion__ (pattern): structural pattern
  that separates interfaces and services so that a generic API calls specific
  services provided by the application; this way the clients of a service don't
  have to be modified if the service is changed or replaced
- __data access object (DAO)__: structural pattern in which a data store is
  accessed through an universal interface that may have multiple implementations
  for different DB types
- __factory__: creational pattern that implements dependency inversion by
  grouping instantiation logic into a separate class called a _factory_; clients
  create new objects indirectly through requests to the factory that returns the
  new object in response
- __dependency injection__: structural pattern that implements dependency
  inversion; clients use a generic service called a _service locator_ that
  either returns a specific service, or forwards requests to a specific service;
  it is different from a factory in that a service locator doesn't necessarily
  return a new service instance
- __callback pattern__: concurrency pattern in which a slow operation is
  launched in the background; a callback function is dispatched to handle the
  output of the operation as soon as the operation is completed
- __scatter/gather pattern__: concurrency pattern in which a task is split up to
  multiple concurrent subtasks; the dispatcher is waiting for all subtasks to
  complete and assembles the result
- __event-driven programming__: architectural pattern in which an _event loop_
  is waiting in an infinite loop to receive asynchronous messages called
  _events_ that it forwards to registered _event handler_ methods
- __debouncing__: ensuring that a function doesn't get called repeatedly so
  frequently that it reduces the performance of the system (e.g. when an event
  is fired again and again)
- __event streaming__: a group of messaging technologies for collecting events
  from _producers_ and distributing them to _consumers_ reliably
- __event sourcing__: messaging pattern in which the application consumes a
  time-ordered sequence of messages that determines the application's state
- __fan-out__: one-to-many messaging pattern in which messages are distributed
  to multiple consumers asynchronously from a message queue, without waiting
  for a response
- __publish–subscribe (pub–sub)__: messaging pattern in which producers send
  messages to logical channels called _topics_ without caring about who the
  consumers are; consumers who subscribe to a topic receive the messages without
  caring about who the producers are
- __circuit breaker__: pattern to ensure that a faulty service doesn't cause the
  same error reoccurring multiple times, by storing the failed state of the
  service until it becomes available
- __command-query responsibility segregation (CQRS)__: architectural pattern
  that uses separate objects or services for retrieving data from (_query_
  service) and modifying data in (_command_ service) the same database
- __model-view-controller (MVC)__: architectural pattern for web applications
  that separates representation of information, storage of data and business
  logic; the _model_ defines the data the system is working with (as a DB
  model), the _view_ creates the output representation of the data, the
  _controller_ retrieves and transforms data that it passes to the view
- __single-page application__: architectural pattern in which the frontend of a
  website is implemented in a similar way to a desktop app; all code is loaded
  when the single web page is loaded, and data is retrieved asynchronously from
  various microservices
- __self-contained systems__: architectural pattern in which each complete
  feature of a web application, including backend and frontend, is implemented
  as a separate service
- __lazy loading__: UI design pattern in which a value of a dataset is only
  loaded when it is accessed by the user
- __eager loading__: UI design pattern in which every value of a dataset is
  loaded when the request is processed
- __over-eager loading__: UI design pattern in which the system anticipates
  what values the user will access from a dataset and pre-loads them
- __typeahead__: UI design approach so that every key press in an input field
  triggers some operation asynchronously, without forcing the user to wait until
  the operation is completed (e.g. search suggestions)

## Testing

- __testing__: investigating a system's completeness, correctness and quality
- __profiling__: measuring a system's performance
- __benchmarking__: making standard measurements to compare one system to
  another or to previous measurements of the same system
- __verification__: investigating whether we are building the system right,
  i.e. the system is correct to specification
- __validation__: investigating whether we are building the right system, i.e.
  this is the right specification
- __quality__: a set of measurable attributes
- __checking__: confirming that a system satisfies certain conditions
- __static testing__: examining the source code without executing the program;
  e.g. code review, static code analysis
- __dynamic testing__: running a program and examining its responses to stimuli
- __testability__: controllability and observability of a system
- __error__: a human action that causes fault
- __fault / defect / bug__: a flaw of the system that causes failure
- __failure / outage__: a deviation of the system from its expected service
- __incident__: one occurrence of a failure
- __malfunction__: the system doesn't meet its specified functionality
- __regression__: a bug that appears after modification of seemingly unrelated
  code
- __latent bug__: a bug that is present but not identified for several releases
- __retesting__: re-executing failed test cases to check the success of a
  correction
- __severity__: the impact of a bug on the end user
- __priority__: the order in which bugs need to be fixed, based on business case
- __test vector__: a set of inputs a component is tested with
- __test oracle__: an information provider on whether the output of the system
  under test is correct or not; e.g. system specification, external test result
  generator
- __white-box testing__: test design based on the internal system structure
- __black-box testing__: test design based on external interfaces of the system
- __exhaustive testing__: test design by exercising all possible input
  combinations and conditions, not possible and/or feasible in most cases
- __risk-based testing__: prioritising test cases according to business value
- __exploratory testing__: when no specification is available, the tester
  attempts to understand the software by experimentation
- __example-based testing__: test design based on test scenarios and test
  vectors chosen manually by the test designer
- __generative testing__: test design by auto-generating test scenarios and/or
  test vectors
- __specification-based testing__: test design by creating a test case for each
  statement of the specification (i.e. for each requirement)
- __pairwise testing__: test design by selecting two critical parameters of the
  system and generating all combinations of the two
- __equivalence partitioning__: example-based test design by splitting the
  inputs into partitions where the behaviour of the software is equivalent, and
  selecting one value from each partition
- __boundary value analysis__: example-based test design by splitting the inputs
  into partitions where the behaviour of the software is equivalent, and
  checking values just below, on, and just above the boundaries of the
  partitions; useful for catching off-by-one errors
- __data flow / basis path testing__: test design by identifying independent
  paths in the control flow graph of the code, and selecting input combinations
  so that each path is executed
- __cyclomatic complexity__: the number of independent paths in the code
- __happy path__: the set of test scenarios that don't involve any _error
  condition_ (negative validator output)
- __edge case__: test case that verifies a _boundary condition_ (upper or lower
  limit of a system parameter)
- __coverage analysis__: test design by finding areas of a program not exercised
  by the existing test cases
- __requirement coverage__: the measure of what percentage of the requirements
  is verified by the full set of test cases
- __traceability matrix__: a table that links each requirement with the test
  cases that verify it for visualising requirement coverage
- __code coverage__: the measure of what percentage of the code is executed by
  running the full set of test cases
- __decision coverage__: the measure of what percentage of independent paths of
  the code is executed
- __statement coverage__: the measure of what percentage of all statements in
  the code is executed
- __model-based testing (MBT)__: generative test design whereby the behaviour of
  the system under test is modeled with a directed graph; each edge represents
  an action and each node represents a verification (assertion); tests cases are
  generated by traversing the graph
- __state space explosion__: the phenomenon that adding new parameters to a
  test model causes exponential growth in the number of states and transitions
- __fault injection__: invoking failures in a running system to test its
  resilience (e.g. network failures, corrupted messages, crashing nodes)
- __fuzzing__: generative test design whereby a large number of test cases is
  generated by randomisation or by applying mutation rules on valid test inputs
- __property-based testing__: generative test design whereby a test case ensures
  an invariant (i.e. a property that is true for all inputs) of the component
  under test via fuzzing
- __shrinking__: automatically reducing the number of test inputs in a long
  generated input sequence to the minimal failing sub-sequence after a failure,
  for easier debugging
- __mutation testing__: addressing weaknesses of the test suite by modifying the
  source code in small ways using _mutation operators_ (e.g. changing each `<`
  to `>`) and checking whether any of the test cases _kills the mutant_ (i.e.
  catches the error)
- __alpha testing__: acceptance testing by the customer on the developer's site
  (in B2B projects) or by a small selected group of users (in B2C projects)
- __beta testing__: acceptance testing on the customer's site (in B2B projects)
  or by a wide audience (in B2C projects)
- __A/B testing__: experiment in which groups of participants are asked to
  complete the same task with different variants of the software and answer a
  questionnaire
- __test harness__: a complete framework for automated testing that consists of
  a test driver, test scripts, test data, test doubles, reporting tools etc.
- __pesticide paradox__: the phenomenon that if the same tests are repeated over
  and over, they will no longer find new bugs as the system under test gets
  optimised for the existing tests
- __heisenbug__: a bug that cannot be reproduced when the program runs in a
  debugger

## Version Control

- __repository__: central database of the version control system, may be
  distributed to several locations
- __revision (SVN) / hash (Git)__: an ID representing the state of a repository
- __log__: a list of descriptions of changes between revisions
- __branching__: storage model that allows creating parallel versions of the
  same file in a repository
- __head__: pointer to the latest revision on the active branch
- __checkout__: obtaining a copy of files stored on a branch for modification
- __commit__: storing changes in the repository, identified by a revision or
  hash
- __revert__: undoing all changes performed by a commit in a new commit
- __export__: obtaining a non-versioned copy of the files of a repository
- __working copy (SVN) / clone (Git)__: a local copy of a remote repository,
  including its entire history
- __update (SVN) / pull (Git)__: updating local files with changes from a
  remote branch
- __fetch (Git)__: pre-loading all changes from a remote repository without
  changing the local copy
- __push__: update a remote branch with changes from a local branch
- __trunk (SVN) / master (Git)__: the main development branch that exists in
  every repository
- __freeze__: denying modification of the source code, to prevent promotion of
  the code from one branch to another
- __staging area__: snapshots of modified files selected for the next commit
- __remote__: a version of the repository hosted on some other computer
- __origin__: the remote server the local copy of the repository was cloned from
- __tag__: a named checkpoint in the history of a branch
- __lightweight tag__: a label attached to a commit as metadata
- __annotated tag__: a label as an object in the repository
- __tracking branch__: a local branch that is following the changes of a remote
  branch
- __upstream__: the remote branch that a local branch is tracking
- __merge__: when branch _A_ is _merged into_ branch _B_, all commits on _A_
  since their common ancestor are applied on top of branch _B_, and the
  resulting commit history replaces branch _B_
- __rebase__: when branch _A_ is _rebased on_ branch _B_, all commits on _A_
  since their common ancestor are applied on top of branch _B_, and the
  resulting commit history replaces branch _A_
- __conflict__: the scenario that the same part of a file gets modified on two
  branches simultaneously; has to be resolved manually when the two branches are
  merged together or rebased on each other
- __stash__: saving changes on the workspace in a temporary area, without
  committing them
- __blame__: identifying which commit changed each line of a file
- __monorepo__: a repository that stores the source code of multiple related
  applications

## Release Engineering

- __continuous integration__: the practice that all developers check their code
  into a common repository, and each commit triggers an automated pipeline of
  static code analysis, compilation, test execution, code coverage measurement
  and profiling
- __continuous deployment__: automated delivery of a software product; the usual
  pipeline is: build, test, package, release, configure, monitor
- __DevOps (development and operations)__: a software engineering approach that
  aims to automate every step of delivery and system administration
- __provisioning__: preparing a device or general-purpose software system to
  provide certain services
- __infrastructure-as-code__: the infrastructure is managed through automated
  deployment of version-controlled configuration files, instead of interactive
  configuration tools
- __SaaS (software as a service)__: software delivery model that includes web
  service hosting for the customer on the developer's servers
- __shared hosting__: hosting multiple websites on a single server
- __virtual hosting / VPS (virtual private server)__: hosting multiple virtual
  machines, each one running a web server, on one physical server
- __bare metal hosting__: web service hosting on the company's own servers;
  cheap but high in complexity
- __cloud hosting__: outsourcing web service hosting to an external company
  (e.g. Amazon Web Services)
- __vertical scaling__: increasing the capacity of a system by adding more
  resources (CPU, memory, disk space) to the existing machines
- __horizontal scaling__: increasing the capacity of a system by adding more
  machines, distributing the workload
- __tenant__: an application that needs its own environment
- __single tenancy__: each tenant has its own dedicated resources (app server,
  database etc.)
- __multi-tenancy__: multiple tenants share the same resources, with some level
  of isolation

## Networks

- __OSI model__: a conceptual model of telecommunication with 7 abstraction
  layers: _physical_ (electrical specification), _data link_ (node-to-node
  transfer), _network_ (addressing and routing), _transport_ (error correction),
  _session_ (establishing permanent connection), _presentation_ (data
  conversion), _application_ (data endpoints)
- __TCP/IP protocol suite__: a set of protocols grouped into 4 abstraction
  layers: _link_ (OSI physical + data link), _internet_ (OSI network),
  _transport_ (OSI transport), _application_ (OSI session + presentation +
  application)
- __internet__: a world-wide network of connected devices that implement (a
  subset of) the TCP/IP protocol stack
- __internet of things (IoT)__: a subset of the internet in which autonomous
  devices are the endpoints of data exchange
- __Wi-Fi (Wireless Fidelity)__: a certification for wireless devices that meet
  certain requirements; not a protocol
- __overlay network__: a network built on top of the internet or some other
  network (e.g. P2P, VoIP, VPN)
- __MAC (media access control) address / physical address__: a globally unique
  identifier assigned to a network device; not routable
- __packet__: the data unit of network transport; consists of a _header_, a
  _payload_ and a _trailer_; each protocol gives a different name to its data
  unit: Ethernet _frame_, IP _packet_, UDP _datagram_, TCP _segment_, ARP & HTTP
  _message_ etc.
- __handshake__: synchronisation mechanism whereby the rules of communication
  are negotiated via message passing between the participants before the
  communication channel is established
- __three-way handshake__: negotiation method for establishing a TCP connection;
  the client sends a connection request (_SYN_ packet), the server responds with
  a confirmation (_ACK_ packet), the client follows up with another confirmation
- __gossip__: message passing technique for peer-to-peer networks whereby each
  machine randomly selects a peer with a given frequency and "spreads" the
  message
- __socket__: an endpoint of a communication channel in a TCP/IP network
- __socket address__: an _IP address_ + _port number_ that together identify an
  application layer process on a network device
- __ephemeral port__: a port number allocated temporarily for the duration of a
  connection from a predefined range; typically used by client processes
- __well-known port__: a permanent port number that is associated with the same
  application layer service on every device by convention; e.g. 22 for an SSH
  server, 53 for a DNS server, 67 for a DHCP server, 80 for a HTTP server
- __loopback interface__: a local-only virtual network interface that sends
  packets back to the host; by convention IP address 127.0.0.1 is assigned to it
  with the host name `localhost`
- __0.0.0.0__: a special purpose non-routable IP address; a server bound to this
  address listens on all available network interfaces
- __network address translation (NAT)__: the process that a router maps an
  external (public) IP address to an internal (private) IP address when it
  delivers an IP packet; allows a group of devices to use the router's external
  IP address, to work around the global shortage of IP addresses
- __bridge / switch__: intermediary that isolates _collision domains_ in an
  Ethernet or Wi-Fi network
- __router / gateway__: intermediary that isolates NAT address spaces in an IP
  network
- __network firewall__: intermediary that isolates internal and external
  networks by filtering data
- __host-based firewall__: software layer that protects the host that it is
  running on by filtering incoming data
- __packet filter__: firewall that blocks traffic from certain hosts and/or
  ports
- __application-level gateway (ALG)__: firewall that understands application
  layer protocols and filters messages by their content
- __proxy__: intermediary that acts like a single client towards a server to
  aggregate requests to the server from multiple clients
- __reverse proxy__: intermediary that acts like a single server towards a
  client to aggregate responses to the client from multiple servers
- __API gateway__: reverse proxy with extended functionality, e.g. response
  caching, request aggregation
- __CDN (content distribution network)__: a network of physically distributed
  proxies that cache content from _origin servers_ close to the clients, and
  protect origin servers from DDoS attacks
- __failure domain__: the section of a network that is affected when a device on
  the network becomes faulty
- __partition__: dividing a network into multiple _subnets_; or: the incident
  that healthy nodes of a network cannot reach each other due to a faulty
  intermediary

## Cryptography

- __Base64__: encoding that represents binary data in ASCII format, reversible
- __token__: a computer-generated number or character sequence used as a
  password or ID
- __nonce (number only used once)__: a single-use token
- __symmetric-key encryption__: the same key is used for encoding and decoding a
  message
- __asymmetric / public-key encryption__: a message can be encrypted by anyone
  using a shared public key, but it can only be decrypted with a paired private
  key
- __checksum__: the hash of a message attached to the message for error
  detection
- __CRC (cyclical redundancy check)__: a checksum calculated as _(predefined
  prime number) / (sum of all message bits) = quotient + remainder_; the
  remainder is used as the checksum
- __message-digest (MD) algorithm__: a cryptographic hash function that is
  _one-way_ i.e. it generates a hash efficiently, but it takes very long to
  reverse
- __SHAx (secure hash algorithm)__: a group of standardised hash functions that
  produce a message digest
- __salt__: random data used as additional input to one-way functions; passwords
  are stored as `[salt, hash(password + salt)]` therefore hashes are unique even
  if passwords are reused
- __Diffie-Hellman (DH) key exchange__: the first party generates public key _A_
  and private key _a_, the second party generates _B_ and _b_, and they exchange
  their public keys; then they both generate the same _session key_ with a
  cryptographic function _f(A,b) = f(B,a)_ that they can use as a key for
  symmetric encryption of a message
- __forward secrecy__: property of key exchange protocols that ensures that
  existing session keys are not compromised even if private keys are
- __RSA (Rivest–Shamir–Adleman)__: asymmetric-key encryption standard that uses
  random large prime numbers for key generation, and supports signed messages
- __AES (advanced encryption standard)__: symmetric-key cipher algorithm with
  128-bit, 192-bit and 256-bit key length variants
- __TLS (transport layer security)__: cryptography protocol that secures
  communication over TCP/IP using RSA for key generation, AES for ciphering, MD5
  or SHA1 for hash generation (among others)
- __block cypher__: encryption of fixed-length groups of bits in multiple
  rounds, each round using a different subkey generated from an original key
- __cryptographic signature__: a method to ensure that a message is from a
  trusted sender; the sender generates a hash and attaches it to the message
  together with a public key; the receiver can verify the validity of the hash
  using the message and the public key
- __certificate__: a signature for verifying the identity of the certificate
  holder
- __proof system__: a machine in which a _verifier_ exchanges messages with a
  _prover_ until the verifier is convinced that the prover is correct
- __zero-knowledge proof__: a method by which the prover is able to prove the
  verifier that they possess a certain piece of information without revealing
  that piece of information
- __HSM (hardware security module)__: a physical device for storing
  cryptographic keys for enhanced security
- __post-quantum__: an algorithm that is secure against an attack by a quantum
  computer

## Distributed Systems

- __transaction__: an instruction that changes the system's state
- __eventuality__: guarantee that an event occurs within a finite amount of
  time, but with no time bound
- __consistency__: property of a distributed system that ensures that the system
  has a state in which all clients see the same data, regardless of which node
  they are connected to
- __availability__: property of a distributed system that ensures that any
  client that sends a request to the system always gets a response
- __partition tolerance__: property of a distributed system that ensures that
  the system is operational even if connection is lost between some of the nodes
- __CAP theorem__: states that a distributed system can only deliver two of
  consistency, availability and partition tolerance
- __CA/CP/AP systems__: types of distributed systems, based on which two of
  consistency, availability and partition tolerance they deliver
- __synchrony__: mode of operation in which for any message sent, the system
  must ensure its delivery within a predefined timeout
- __asynchrony__: mode of operation in which each message must eventually be
  delivered
- __partial synchrony__: mode of operation that defines a recurring _Global
  Stabilisation Time_ after which all messages must be delivered within a
  predefined timeout; before the GST event the system behaves asynchronously
- __finality__: the state of a transaction in which it cannot be revoked any
  more; or the property that guarantees the existence of such state in the
  system
- __safety__: property that guarantees that some events never happen in the
  system
- __liveness__: property that guarantees that some events eventually happen in
  the system
- __fault tolerance__: property of a distributed system that ensures that the
  system is able to maintain normal operation even if some of the nodes go down
- __Byzantine fault__: malfunction of a node by sending arbitrary (fake)
  messages
- __Byzantine fault tolerance (BFT)__: property of a distributed system that
  ensures that the system is able to maintain normal operation even if some of
  the nodes act maliciously (i.e. send fake messages)
- __BFT bounds__: for _N_ total number of nodes and _f_ faulty nodes, BFT can
  only be achieved if _f < N/2_ in synchronous systems, and _f < N/3_ in
  partially synchronous systems
- __consistency model__: a set of safety and liveness guarantees on how
  transaction history is recorded
- __strict consistency__: consistency model that ensures that all clients see
  the same data, regardless of which node they are connected to, at any point of
  time
- __eventual consistency__: consistency model that allows that clients receive
  inconsistent data at some point of time, but ensures that data is eventually
  synchronised between nodes
- __serializability__: consistency model that ensures that for any set of
  concurrent transactions it is possible to find a linear sequence of
  transactions with equivalent outcome
- __sequential consistency__: consistency model that ensures that the order of
  transactions is deterministic and the same on each node
- __linearizability__: consistency model that ensures that transactions are
  _atomic_ and preserve wall-clock order; e.g. if transaction _A_ is committed
  before transaction _B_, and transaction _A_ writes _x_, then transaction B is
  able to read _x_
- __consensus__: a group decision on the validity of a proposed new system
  state; ensures data coherence between nodes of a distributed system
- __validity / integrity__: safety property of consensus that ensures that if
  a proposal is valid then a honest participant will vote on that proposal
- __agreement__: safety property of consensus that ensures that all honest
  participants always vote on the same proposal (i.e. there cannot be two
  competing variants of the database)
- __termination__: liveness property of consensus that ensures that all honest
  participants eventually vote on some proposal
- __quorum__: the minimum number of participants required for a distributed
  system to be able to reach consensus
- __no quorum__: the event that a distributed system doesn't have enough
  participants to reach consensus
- __FLP impossibility__: theorem that states that an asynchronous distributed
  system cannot deliver all three of safety, liveness and fault tolerance;
  consequence: in an asynchronous system there is no deterministic way to reach
  consensus with even a single faulty node

## Databases

- __ACID (atomicity, consistency, isolation, durability)__: properties a
  database transaction should comply with
- __atomicity__: property of a storage system that ensures that a transaction is
  a single unit that either completely succeeds, or completely fails and keeps
  the system's state unchanged
- __consistency__: property of a storage system that ensures that all of its
  clients see the same data
- __isolation__: property of a storage system that ensures that all of its
  transactions are serializable; consequence: concurrent transactions don't
  impact each other's execution
- __durability__: property of a storage system that ensures that finalised
  transactions are persistent (are not lost in case of a failure)
- __multiversion concurrency control (MVCC)__: database synchronisation method
  where each data record is versioned; a transaction is working with a
  _snapshot_ of the database, and an update creates a new version of a record
  instead of replacing it
- __partial persistence__: persistence model where any version of a data record
  can be read, but only the latest version can be updated
- __full persistence__: persistence model where any version of a data record can
  be read and updated
- __state machine replication__: database synchronisation method where each node
  implements the same deterministic state machine, and receives the same inputs
  in the same order, thereby transitioning into the same state and producing the
  same output
- __CRUD (create, read, update, delete)__: the basic operations a persistent
  storage system must implement
- __data warehouse__: a database in an information system used for data analysis
  and reporting
- __ETL (extract, transform, load)__: data warehousing process that involves
  extracting data from external sources, transforming data into an internal
  storage format, and loading data into a target database
- __key-value store__: persistent storage of hash tables; entities are
  represented as unstructured values (e.g. Redis)
- __document store__: special case of key-value stores where values (called
  _documents_) have transparent internal structure that is exposed by the
  database API, providing access to attributes (e.g. MongoDB)
- __information retrieval (IR) system__: document store optimised for search
  performance (e.g. Elasticsearch)
- __relational database__: storage system in which entities are represented as
  rows and attributes are represented as columns of tables with predefined
  structure; relationships are indirect via links to rows of other tables (e.g.
  Oracle, Postgres)
- __graph database__: storage system in which entities are represented as nodes
  of a graph, attributes are represented as properties of nodes, and
  relationships are represented as edges of the graph; nodes may be grouped
  together using labels (e.g. Neo4j)
- __NoSQL database__: any type of non-relational database
- __distributed hash table (DHT)__: distributed system that implements a
  key-value store; nodes form an overlay network and a hash function maps keys
  to shards
- __sharding__: horizontal partitioning of data, i.e. database records are
  distributed among separate database servers called _shards_; improves search
  performance
- __back pressure__: mechanism for limiting the number of unprocessed (queued)
  messages in a distributed system to prevent overloading
- __blob (binary large object)__: unstructured binary data stored in a database
  record (e.g. an image or audio file)
- __yield__: metric of a DB system; the percentage of requests answered
  successfully
- __harvest__: metric of a DB system; the percentage of required data included
  in responses

## Relational Databases

- __dump__: backup of a database by recreating its tables and filling with data
  using SQL commands
- __hot-copy__: backup of a database by copying its data files
- __stored procedure__: subroutine executed by the DBMS
- __cursor__: temporary area created in the system memory when an SQL statement
  is executed; stores the data retrieved from the database
- __view__: virtual table produced by a stored query
- __trigger__: a subroutine that is automatically executed when certain events
  happen with a certain view, table or database
- __index__: internal data structure that speeds up lookups in a table based on
  some of the columns
- __schema__: model of the database structure, including tables, views, indexes,
  etc.
- __rollback segment__: area of the database where temporary data of a
  transaction is stored; its contents are saved to the database with the
  _commit_ command or discarded with the _rollback_ command
- __primary key__: a column whose value uniquely identifies an entire row; a
  table can have one primary key, cannot be `null`
- __surrogate key__: a system-generated primary key with no real-world meaning
- __secondary key__: an alternative candidate for primary key, not required to
  be unique
- __foreign key__: a column of a table that is primary key in another table,
  used to link tables
- __join__: command that combines two (or more) tables of a database
- __cross join__: Cartesian product of two tables (_M×N_ rows); combines every
  record of one table with every record of the other table
- __inner join__: creates the cross join of two tables and returns those rows
  where the two tables have matching records according to the join condition
- __natural join__: inner join in which the join condition is implicit; matches
  the columns that have the same name in both tables
- __outer join__: creates the union of two tables and returns all rows of at
  least one of the tables, even if the join condition is not satisfied; the
  records from the other table contain null values in that case
- __left outer join__: all records of the left table (_M_ rows), paired with
  records from the right table if the join condition is satisfied
- __right outer join__: all records of the right table (_N_ rows), paired with
  records from the left table if the join condition is satisfied
- __full outer join__: all records of both tables (max. _M+N_ rows), paired if
  the join condition is satisfied
- __normalisation__: arranging unstructured data into multiple tables to reduce
  redundancy and data inconsistency
- __ORM (object-relational mapping)__: a database abstraction layer that
  transforms the database into an object model, and hides database queries by
  methods of an object oriented language
- __active record__: architectural pattern for implementing ORM; a table is
  represented by a class, and a row by an object; the DB is kept in sync with
  the object's state

## Graph Databases

- __node__: represents an entity (object)
- __label__: a description attached to a node; groups nodes with the same label
  into a _set_
- __relationship__: an arc (directed edge); represents an action
- __relationship type__: a label attached to a relationship
- __property__: a key-value pair attached to a node or a relationship
- __index__: internal data structure that speeds up finding nodes with a
  certain label based on some of its properties
- __constraint__: a rule that data in the graph must comply with (e.g. the
  values of some property must be unique)

## Web Development

- __World Wide Web__: an information sharing model over the internet, based on
  the HTTP protocol and accessed via web browsers
- __REST (representational state transfer)__: communication protocol in which
  standard HTTP methods and errors are used for data exchange between arbitrary
  nodes of a distributed system
- __thin client__: the majority of data processing happens on the server
- __thick / fat client__: the majority of data processing happens on the client
  (i.e. in the web browser)
- __session__: activity between a client's first request and the expiry of an
  associated inactivity timer on the server; the server can assign a temporary
  data storage to a session
- __cookie__: a small piece of data associated with a server that is stored on
  the client, and included in the headers of requests to the server
- __authentication__: verifying a user's identity
- __authorisation__: assigning rights to a user based on their identity
- __basic access authentication__: the user name and password are sent in the
  HTTP request header without encryption (base64-encoded); TLS may be used to
  secure messages
- __digest access authentication__: challenge-response method based on exchange
  of MD5 encrypted values: the server sends a nonce to the client; the client
  responds with _MD5(nonce + MD5(shared secrets))_; the server also calculates
  the same hash and grants or denies access
- __session-based authentication__: stateful access method; after access is
  granted, the server sends an access token (_session ID_) to the client; the
  server remembers the token and uses it to identify the client in subsequent
  requests until the session expires
- __token-based authentication__: stateless access method; after access is
  granted, the server sends an access token to the client; the server
  recalculates the token for each subsequent request by the client
- __JWT (JSON web tokens)__: token-based authentication standard where the
  access token is a JSON encoded string with a standardised format
- __OAuth__: token-based authentication standard for delegating authentication
  to a third-party service (e.g. Google, Facebook); the third-party service
  issues the access token
- __DOM (Document Object Model)__: a tree representing the elements of a HTML
  document with an API provided by web browsers that enables modification of the
  elements
- __XMLHttpRequest (Ajax)__: an API available for web browser scripting that
  makes it possible to send HTTP requests to the server without reloading the
  page
- __semantic HTML__: HTML tags that have meaningful, descriptive names like
  `footer`, `table`, `code` vs. _non-semantic_ tags like `div`, `span`
- __synchronous request__: the program halts after a request is sent until the
  response is received
- __asynchronous request__: the program continues after a request is sent,
  response is evaluated by a callback function
- __over-fetching__: sending unnecessary data from the server to the client
- __under-fetching__: the client requests data that the server doesn't know
  about
- __template__: HTML document that contains special named tags that are replaced
  dynamically when processing the page
- __slug__: a human-readable identifier of a resource on the server (e.g. an
  article is identified by its sanitised title in the URL rather than its ID)
- __transclusion__: inclusion of a document in other documents through
  references (e.g. including a HTML template into another template)
- __query string__: the part of a URL containing HTML form data in the format
  `?<key>=<value>&<key>=<value>&<...>`
- __same origin policy__: security concept for web browsers that permits browser
  scripts to access content originating from the same domain the script was
  loaded from; content from other domains is blocked
- __SSE (server sent events)__: persistent connection in which the client keeps
  the connection to the server open after the first HTTP request, and processes
  the body of the HTTP response from the server in chunks as they are sent
- __WebSocket__: persistent connection that is negotiated via HTTP messages, but
  data exchange is through a raw bidirectional TCP socket (using the HTTP port)
- __CGI, WSGI (Python), Rack (Ruby), Servlet (Java)__: standard interfaces
  between the web server and web frameworks; wrap HTTP requests and responses
- __CGI (common gateway interface)__: the most simple web server interface;
  launches a new interpreter process for every HTTP request, request parameters
  are passed into the process in environment variables, the app is expected to
  print the HTTP response to standard output
- __bundler__ / __file loader__: a tool that bundles all JavaScript and CSS
  source files, including dependencies, into a single file to improve page
  loading time (e.g. Webpack)
- __web crawler__: a robot that follows all hyperlinks on a website to build a
  search index of the pages
- __sitemap__: a standardised XML file that tells web crawlers about the pages
  available on the server
- __robots.txt__: a file that tells web crawlers which locations on the server
  to ignore and where to find the sitemap

## Web Security

- __deep web__: subset of the world wide web that is not indexed by search
  engines
- __dark web__: subset of the world wide web that is accessible via protected
  overlay networks (e.g. Tor)
- __penetration testing__: evaluating the security of a system by simulated
  attacks
- __white hat hacking__: ethical hacking for the purpose of penetration testing
- __red team__: cybersecurity approach that focuses on penetration testing
  (imitating an attacker)
- __blue team__: cybersecurity approach that focuses on threat profiling and
  defense mechanisms
- __DDoS (distributed denial-of-service) attack__: a large number of clients
  flood the bandwidth of a targeted web server
- __brute force attack__: the attacker tries to authenticate using
  trial-and-error method
- __man-in-the-middle (MITM) attack__: the attacker secretly intercepts the
  communication channel between two parties and modifies the messages
- __XSS (cross-site scripting)__: injecting malicious scripts exploiting
  insufficient validation that are included in the response and run on the
  client
- __command injection__: injecting malicious commands exploiting insufficient
  validation that run on the server
- __CSRF (cross-site request forgery)__: injecting malicious scripts that cause
  the user's browser to perform actions on a website where the user is logged in
- __insufficient authentication__: a website permits an attacker access to
  sensitive data because of loose access control
- __DNS hijacking__: redirecting the DNS resolution requests to a bogus DNS
  server that translates legitimate names to addresses of malicious sites
- __social engineering__: psychological manipulation of users of a system
- __phishing__: attempt to acquire sensitive information by acting as a
  trustworthy contact
- __doxing__: collecting and releasing sensitive personal information
- __spoofing__: sending IP packets with false source IP address
- __beacon__: an object invisibly embedded in a web page or e-mail for tracking
  the activity of the user who has accessed the content

## HTTP Methods

- __HEAD__: request a response header without response body
- __GET__: request a resource
- __POST__: submit data in the request body to create a resource, non-idempotent
  i.e. submitting the same data twice creates two resources
- __PUT__: submit data in the request body to create a resource, idempotent i.e.
  submitting the same data a second time doesn't have effect
- __PATCH__: apply partial modification on a resource
- __DELETE__: remove a resource from the server
- __TRACE__: echo back the request in the response
- __OPTIONS__: return the HTTP methods that the server supports
- __CONNECT__: build up a tunnel for TLS encrypted communication

## XML

- __metadata__: information on properties of data
- __markup__: annotation with the purpose of storing metadata in a document
- __start-tag__: markup that starts with `<` and ends with `>`
- __end-tag__: markup that starts with `</` and ends with `>`
- __content__: textual information between a start tag and an end tag
- __element__: a start-tag + content + end-tag representing a node
- __attribute__: a key – value pair inside a start tag
- __schema / XSD (XML schema document)__: a set of rules, e.g. markup
  declarations that define a specific type of XML document
- __doctype__: instruction that associates an XML document or web page with a
  schema
- __XSLT (extensible stylesheet language transformations)__: declarative
  language that transforms XML documents between schemas
- __XPath__: query language for selecting nodes from XML documents
- __axis__: defines a set of nodes relative to the current node in XPath

## Regular Expressions

- __local matching__: finding the first string that satisfies the regular
  expression
- __global matching__: finding all strings that satisfy the regular expression
- __greedy matching__: matching the longest string that satisfies the regular
  expression
- __non-greedy matching__: matching the shortest string that satisfies the
  regular expression (`?` after the quantifier)

## gRPC

- __protocol buffer (protobuf)__: binary serialisation format used by _gRPC_;
  the message and service structure is defined in the _Protocol Buffers
  language_, from which data access classes can be generated in any supported
  language
- __gRPC__: Google's RPC protocol; services and messages are defined in protocol
  buffer files, the server implements the service while the client implements a
  stub, and the gRPC API is responsible for message passing
- __binary framing__: a layer of HTTP 2.0 that breaks down HTTP 1.x messages
  into small binary-encoded _frames_ at the sender and reassembles them at the
  receiver
- __streaming__: sending multiple HTTP messages within the same TCP connection
- __simple RPC__: the client sends a request to the server and waits for a
  response
- __server-side streaming RPC__: the client sends a request to the server and
  receives a stream of messages; waits until there are no more messages
- __client-side streaming RPC__: the client sends a stream of messages to the
  server and waits for a response
- __bidirectional streaming RPC__: both sides send a stream of messages
  independently

## React

- __JSX__: a JavaScript extension that allows combining HTML templates with
  JavaScript code
- __React DOM__: an object model React maintains, and updates in the HTML DOM
  when necessary
- __element__: an equivalent of a HTML DOM element in the React DOM, stored as a
  JavaScript object
- __rendering__: creating a DOM element instance
- __mounting / unmounting__: attaching a React element to / removing a React
  element from the HTML DOM
- __component__: a reusable part of the UI that can be referred to as a custom
  HTML element
- __class component__: component implemented as a subclass of `react.Component`
- __functional component__: component implemented as a function, returns the
  HTML code
- __props__ (properties): input parameters of a component, defined as HTML
  attributes, immutable
- __state__: an object tree that holds the local state of a component, can only
  be updated through the `setState` method of the component
- __hook__: a context-specific function that provides access to some of the
  attributes of the equivalent class component in a functional component
- __ref__: a reference to a child DOM element or component that is not included
  in the _props_ of a component
- __controlled component__: a HTML form element whose value is bound to the
  internal state of the containing React component
- __uncontrolled component__: a HTML form element whose value can be accessed
  via a _ref_
- __synthetic event__: a React wrapper over a native DOM event

## Redux

- __store__: an object tree that holds the global, immutable state of an
  application; it can only be updated through _actions_
- __action__: an event as an arbitrary object that is dispatched whenever
  changing the global state is required
- __action creator__: a factory function that returns an action
- __reducer__: a function with `(state, action) => state` signature that
  transforms the current state into a new state as a result of an action
- __root reducer__: the reducer that updates the entire store, may be decomposed
  to multiple reducers that only update a subtree
- __representational component__: a React component that describes how a part of
  the UI looks
- __container component__: a React component that connects props of
  representational components to the Redux store and Redux actions

## Docker

- __cgroups (control groups)__: Linux feature that isolates all resources of a
  group of processes while sharing the same Linux kernel; allows creating
  _containerized_ applications
- __container__: an isolated user space and file system with limited access to
  I/O devices, for packaging and safeguarding an application and its execution
  environment together
- __image__: a read-only snapshot of a container, composed of multiple layers of
  _intermediate images_
- __Dockerfile__: a sequence of commands as a recipe for creating an image

## Kubernetes

- __pod__: the unit of execution in Kubernetes; an application container, or
  multiple containers with shared resources; has its own internal IP address
- __node__: a machine managed by Kubernetes that is running one or more pods
- __service__: an abstraction layer that defines a set of pods, their scaling
  and their access policy (e.g. internal IP, NAT, load balancer)
- __cluster__: a group of nodes that runs all services of a system
- __master__: a machine that coordinates all activities of nodes of a cluster
- __Kubelet__: the Kubernetes process that communicates with the master, running
  on every node
- __rolling update__: pods running a service are updated incrementally to ensure
  zero downtime
- __ingress__: mapping incoming traffic from the internet to services running in
  a cluster

## Blockchains & Bitcoin

- __cryptocurrency__: a system of money whose transactions are secured by
  cryptographic methods instead of supervision by a central governing body
- __distributed ledger__: a decentralised data store for financial transactions
- __token__: an entry in a cryptocurrency ledger with monetary value
- __blockchain__: a state machine that implements a distributed ledger that is
  replicated across nodes of a peer-to-peer network, with mechanisms that make
  it fault-tolerant and immutable (append-only)
- __public blockchain__: that anyone can read and write (e.g. Bitcoin, Ethereum
  mainnet)
- __private blockchain__: that only trusted nodes can write to
- __native token__: created within the blockchain as some form of incentive to
  validate transactions (e.g. BTC, ETH)
- __asset-backed / security token__: an IOU backed by an external asset (e.g.
  USD) as collateral and an external regulator
- __stablecoin__: an asset-backed token whose value is pegged to a non-volatile
  asset (e.g. USD, gold)
- __UTXO (unspent transaction output)__: a set of tokens that a user received as
  an output of a transaction and is able to send later; the user's balance is
  defined by the sum of their UTXOs (i.e. the history of their transactions)
- __transaction__: the transfer of ownership of a digital token, stored in a
  Merkle tree structure and attached to a block of the chain
- __block__: a record in the database that contains a bunch of transactions and
  metadata; each block has a reference to the previous block via its hash,
  thereby creating a chain
- __genesis__: the initial state of a blockchain; contains a single block that
  doesn't include any transaction
- __full node__: a node that downloads the full blockchain; is able to create
  new blocks
- __light node__: a node that downloads only the chain of block headers; is able
  to verify the validity of transactions
- __account address__: a public key that identifies an account
- __wallet__: a storage of public/private key pairs
- __HD (hierarchical deterministic) wallet__: a 12-word-long _seed phrase_
  (mnemonic) is used to generate a parent key from which child keys can be
  derived; this way a mnemonic identifies multiple accounts
- __longest chain rule__: consensus rule that states that if nodes have
  different blocks at the end of their chains, the chain with the largest number
  of blocks is considered the legitimate chain
- __Sybil attack__: overtaking a blockchain network by creating so many
  coordinated malicious nodes with a corrupted blockchain that they form the
  majority of the network
- __proof of work (PoW)__: consensus mechanism that prevents a Sybil attack;
  multiple nodes compete to be able to propose the next block by trying to solve
  a computationally difficult challenge first, which makes a Sybil attack too
  expensive as it would require remining all blocks after the corrupted one
- __Nakamoto consensus__: Bitcoin's consensus protocol based on the longest
  chain rule and a proof of work challenge in which the hash of a new block must
  be smaller than a predefined number, which makes it computationally difficult
  to create one as it requires lots of guesses
- __mining__: adding new blocks to the blockchain that pass the proof-of-work
  challenge; as mining is computationally difficult, it is rewarded by tokens
- __mining difficulty__: the computational difficulty of creating a new block is
  continuously optimised so that one block is added to the Bitcoin blockchain
  roughly every 10 minutes and to the Ethereum blockchain every 15 seconds
- __pre-mined tokens__: tokens that are created at the time a new blockchain is
  launched and not via mining
- __coinbase transaction__: the transaction that rewards the successful miner of
  a new block
- __mempool__: temporary storage of pending transactions that are waiting to be
  mined into a block
- __orphan__: a block that was not the first to be mined for a set of
  transactions; gets discarded
- __proof of authority (PoA)__: consensus mechanism for private blockchains in
  which the _proposer_ of the next block is selected using round-robin
  scheduling or completely random, and the new block has to be accepted by a
  quorum of approved nodes called _validators_
- __proof of stake (PoS)__: similar to PoA, but each validator's voting power
  and its frequency of being selected as a proposer depend on the fraction of
  tokens in the system that it owns (its _stake_ in the system); this way an
  actor with >50% share doesn't have an incentive to act maliciously
- __slashing__: deleting the deposits of a validator in a PoS system if they
  violate consensus rules
- __sidechain__: a blockchain that can transfer tokens to and from a separate
  blockchain called the _main chain_; increases transaction throughput
- __state channel__: a mechanism in which participants send transactions to each
  other off the chain and submit the final state back to the chain; increases
  transaction throughput
- __hard fork__: a permanent divergence in the blockchain due to nodes of the
  network following different consensus rules
- __soft fork__: a temporary divergence in the blockchain that occurs when a
  change in consensus rules is rolled out to the network

## Ethereum

- __smart contract__: a program distributed across and executed by all nodes of
  a blockchain network; its execution _state_ and _receipt_ (result) are stored
  in Merkle trees and attached to a block of the chain
- __app token__: a cryptocurrency that is implemented by a smart contract
- __Đapp (decentralised application)__: smart contracts + a frontend (GUI or
  API) for using the contracts
- __DAO (decentralised autonomous organisation)__: a virtual legal entity
  implemented on the Ethereum network based on interacting contract accounts
- __oracle__: a service that a smart contract can call to request data from the
  outside world (outside the blockchain) for a transaction fee
- __uncle / ommer__: an Ethereum block that was not the first to be mined for a
  set of transactions; can still be referenced by later blocks
- __Wei__: Ethereum's smallest native token unit
- __Ether__: Ethereum's default native token unit; 1 ETH = 10<sup>18</sup> Wei
- __gas__: the unit of Ethereum's block size; the measure of the complexity of a
  smart contract function
- __gas price__: the transaction fee in the Ethereum network paid to prevent
  hostile injection of expensive (long-running) applications; the amount of ETH
  to spend on each gas of computation
- __gas limit__: the max. amount of ETH the sender is willing to spend on a
  smart contract execution
- __externally owned account (EOA)__: a public/private key pair with an
  associated balance (instead of _UTXO_), storage and transaction counter; can
  sign transactions
- __contract account__: a public key with an associated balance, storage,
  transaction counter and code that other accounts can request to execute
- __external transaction__: a message from an externally owned account
- __internal transaction__: a message from a contract account
- __contract transaction__: a message to a contract account that invokes a
  contract function; changes the blockchain state and consumes gas
- __contract call__: a message to a contract account that invokes a contract
  function; doesn't change the blockchain state and doesn't consume gas
- __contract constructor__: a special smart contract function that gets called
  when the smart contract is deployed to the blockchain
- __falback function__: a special smart contract function that gets called if
  someone sends Ether to a smart contract, but calls a non-existent contract
  function
- __full sync__: synchronisation method in which a client downloads and executes
  (validates) all blocks since the genesis
- __fast sync__: synchronisation method in which a client downloads all blocks,
  then selects a recent block (the _launch block_) and only executes the blocks
  that have been created since the launch block
- __layer 1__: the group of protocols related to the Ethereum core
- __layer 2__: the group of protocols built on top of Ethereum, e.g. state
  channel protocols
- __EVM (Ethereum virtual machine)__: virtual machine that runs smart contracts;
  provides a stack, a temporary byte-array memory and a permanent key-value
  store; executes EVM bytecode
- __RLP (recursive length prefix)__: the serialisation format of Ethereum's
  transactions; for serialising nested arrays of binary data
- __JSON-RPC__: API specification for interacting with Ethereum nodes over HTTP
  or WebSocket
- __beacon chain__: the blockchain responsible for controlling PoS consensus and
  coordinating the 64 shards that the mainnet consists of in Ethereum 2.0

## Tendermint

- __validator__: a node that participates in the consensus decision making
- __propose step__: the next _proposer_ is selected using weighted round-robin
  scheduling; the proposer broadcasts a _proposal message_ with a new block (or
  the most recent valid block from a previous round) to all validators
- __pre-vote step__: if a validator receives the proposed block and ensures its
  validity, or the _propose timer_ expires, then it broadcasts a _pre-vote
  message_ with the block ID (acceptance) or _nil_ (rejection or timeout)
- __polka__: the event that more than 2/3 of all validators (in voting power)
  pre-vote for the proposed block
- __pre-commit step__: if a validator receives a polka or the _pre-vote timer_
  expires then it broadcasts a _pre-commit message_ with the block ID
  (acceptance) or _nil_ (rejection or timeout)
- __lock__: a validator is _locked_ on the block it pre-commits, i.e. it will
  pre-vote for the same block in subsequent rounds, until the block is committed
  or the validator receives a polka for a different block in a later round
- __commit__: if a validator receives pre-commit messages for the proposed block
  from more than 2/3 of all validators (in voting power) before the _pre-commit
  timer_ expires and the block is valid then it accepts the block and increments
  the _height_ (block counter)
- __gossip round__: consists of propose → pre-vote → pre-commit state
  transitions; multiple rounds may be required for a block to be committed

## GStreamer

- __element__: a plug-in application in the signal processing chain
- __bin__: a set of elements that can act like a single element
- __pipeline__: the top-level bin that contains the entire chain
- __pad__: a connection point of an element that may link the element to another
  element of the pipeline
- __source pad__: emitter of A/V signals
- __sink pad__: receiver of A/V signals
- __dynamic pad__: a pad attached to an element in runtime and may be deleted on
  certain conditions (e.g. end-of-stream event)
- __static pad__: a pad that is always available
- __ghost pad__: a pad of a bin that provides access to a pad of an element
  inside the bin
- __sink element__: an element that has no source pads
- __source element__: an element that has no sink pads
- __live source__: a source element that discards data when it's not running,
  and timestamps buffers with the current running time of the pipeline
- __caps (capabilities)__: media types a pad is compatible with (e.g. audio, raw
  video, compressed video)
- __bus__: the message handler of a pipeline
- __buffer__: an A/V sample bundled with some metadata (e.g. timestamp),
  transmitted via pads
- __frame__: a still image of the video, included in a buffer
- __message__: an arbitrary piece of metadata that elements send to a bus
- __event__: a piece of metadata that elements send together with the buffer
  flow to signal a special condition, e.g. end-of-stream (`EOS`)
- __appsink__: a sink element that injects buffers into an external application
- __appsrc__: a source element an external applicaton injects buffers into
