# Music Theory Notes

## Music

- __melody__: an arrangement of successive notes
- __chord__: a combination of notes played simultaneously
- __harmony__: an arrangement of successive chords
- __voice__: a melodic part in a specific tonal range
- __texture__: the relationship between voices played simultaneously
- __counterpoint__: the study of harmonic relationships between melody lines
- __dyad__: a 2-note chord
- __triad__: a 3-note chord

## Sheet

- __note / tone__ = pitch + duration
- __score__ = tempo + rhythm + melody + harmony + dynamics
- __stave / staff__: 5 parallel lines for visualising notes
- __ledger line__: extension line above or below the staff
- __brace / `{`__: notation symbol that groups staves representing multiple
  ranges of an instrument
- __grand stave__: two staves joined by a brace, to be played at once

## Clefs

- __clef__: indication of which notes the lines of the stave represent
- __treble clef / G-clef__: G is on the 2nd line of the stave
- __bass clef / F-clef__: F is on the 4th line of the stave
- __C-clefs__: C is on the 3th line of the stave
- __accidental__: a pitch modifier (a sharp or a flat or a natural)
- __sharp / ♯__: raises the note by a semitone until the end of bar
- __flat / ♭__: lowers the note by a semitone until the end of bar
- __natural / ♮__: cancels a previous sharp or flat until the end of bar

## Rhythm

- __rest__: an interval of silence with a specified duration
- __pause / fermata__: an out of time rest; its length is chosen by the player
- __bar / measure__: a group of notes with the total duration of a given number
  of beats
- __beam__: line that connects adjacent notes in the sheet to simplify notation
- __tie__: indication of extending the duration of a note by the duration of the
  tied note
- __dot__: indication of extending the duration of a note by half of its
  original duration
- __double dot__: indication of extending the duration of a note by
  three-quarters of its original duration
- __tuplet__: division of a whole note by a number that is not a power of 2
- __triplet__: division of a whole note by 3
- __meter__: a fraction as the number of beats in a bar over the length of a
  beat
- __common time__: 4/4 meter
- __simple meter__: the number of beats in a bar is a power of 2
- __compound meter__: any other meter
- __downbeat / on-beat__: the odd beats of a bar
- __upbeat / off-beat__: the even beats of a bar
- __backbeat__: accenting the upbeats (beats 2 and 4) in common time
- __syncopation__: a rhythm that doesn't follow the beat
- __anacrusis__: a piece of music starts at the last beat of a bar called the
  __pickup note__
- __tenuto__: holding the sound for the entire duration of the note
- __staccato__: cutting the sound short and keeping a rest for the remaining
  duration of the note

### Polyrhythm

- __polyrhythm__: multiple rhythms with different subdivisions played
  simultaneously
- __X:N polyrhythm / cross-rhythm__: two rhythms with different subdivisions
  played simultaneously, one subdivides the bar to N beats (primary pulse) and
  the other to X beats (secondary pulse)
- __hemiola__: a 3:2 (or 2:3) polyrhythm

#### Building an X:N polyrhythm

1. create a bar with *N* primary beats (the primary pulse)
2. subdivide each primary beat into *X* beats
3. accent every *Nth* subdivision
4. remove the non-accented notes to get the secondary pulse

## Tempo

- __lento, largo__: very slow (~40 bpm)
- __adagio__: slow (~60 bpm)
- __andante__: at a walking pace (~90 bpm)
- __moderato__: moderately fast (~110 bpm)
- __allegro, vivace__: fast (~140 bpm)
- __presto__: very fast (~180 bpm)
- __a piacere__: the tempo is up to the performer
- __tempo giusto__: strict tempo
- __tempo rubato__: free tempo; the performer is allowed to freely “stretch” or
  “squeeze” the tempo of a phrase
- __accelerado__: gradual increase in tempo
- __ritardando__: gradual decrease in tempo

## Dynamics

- __accent__: emphasis on one particular note
- __piano__: soft
- __forte__: strong
- __mezzo~__: moderately
- __~issimo__: very much
- __sforzando__: sudden increase in loudness
- __crescendo__: gradual increase in loudness
- __decrescendo__ / __diminuendo__: gradual decrease in loudness

## Ornaments / decorations

- __legato__: connecting the subsequent notes smoothly, without separation
- __slur__: symbol in the sheet that groups notes to be played legato
- __glissando / slide__: sliding from a pitch to another
- __grace note__: a single note ornament
- __acciaccatura / crush note__: a grace note played fast, doesn't take away
  from the decorated note's duration
- __appoggiatura__: a grace note played slow, usually halves the decorated
  note's duration
- __trill__: quickly alternating between the decorated note and a note above it
- __mordent__: playing a note, quickly playing a note above it, and returning to
  the decorated note
- __vibrato__: varying the pitch of an extended note rapidly, which results in a
  pulsating effect

## Solfège

- __sight-reading__: performing written music unseen before
- __aural skills__: the ability to identify the notes of a musical piece unheard
  before
- __fixed do system__: do is always C
- __movable do system__: do is the tonic
- __la based minor__: do is the tonic of the relative major (ionian) scale
- __do based minor__: do is the tonic of every scale
- __Kodály method__: movable do with la based minor and rhythm syllables

## Relative durations [with rhythm syllables]

- 1 = whole note = __semibreve__ [ta-ah-ah-ah]
- 1 1/2 = dotted 1/2 note [ta-ah-ah]
- 1/2 note = __minim__ [ta-ah]
- 3/8 = dotted 1/4 note [tam]
- 1/4 note = __crotchet__ [ta]
- 1/8 note = __quaver__ [ti]
- 1/16 note = __semiquaver__ [tiri]
- 1/32 note = __demisemiquaver__
- 1/64 note = __hemidemisemiquaver__
- 1/3 note = __triplet__ [tri-o-la]

### Counting

- 1/4 subdivision: _1 2 3 4_
- 1/8 subdivision: _1 & 2 & 3 & 4 &_
- 1/16 subdivision: _1 e & a 2 e & a 3 e & a 4 e & a_

## Intervals

- __diatonic interval__ = quality of the interval + the number of diatonic steps
- __compound interval__: interval bigger than an octave
- __pitch class__: the set of all notes an octave apart (e.g. all C notes)
- _0 semitone_: perfect 1st = diminished 2nd = prime = unison
- _1 semitone_: minor 2nd = augmented 1st = semitone (s)
- _2 semitones_: major 2nd = diminished 3rd = tone (T)
- _3 semitones_: minor 3rd = augmented 2nd
- _4 semitones_: major 3rd = diminished 4th
- _5 semitones_: perfect 4th = augmented 3rd
- _6 semitones_: diminished 5th = augmented 4th = tritone
- _7 semitones_: perfect 5th = diminished 6th
- _8 semitones_: minor 6th = augmented 5th
- _9 semitones_: major 6th = diminished 7th
- _10 semitones_: minor 7th = augmented 6th
- _11 semitones_: major 7th = diminished 8th
- _12 semitones_: perfect 8th = augmented 7th = octave

### Harmonic classification of intervals

- __unison__
- __perfect consonance__: perfect 5th, octave
- __imperfect consonance__: major or minor 3rd or 6th
- __dissonance__: all other intervals

### Inverted intervals

- interval + inverted interval = 9
- 2nd → 7th
- 3rd → 6th
- 4th → 5th
- 5th → 4th
- 6th → 3rd
- 7th → 2nd
- minor → major
- major → minor
- augmented → diminished
- diminished → augmented
- perfect → perfect

## Tonal degrees

### Diatonic

- _P1_: do / tonic
- _M2_: re / supertonic
- _M3_: mi / mediant
- _P4_: fa / subdominant
- _P5_: sol / dominant
- _M6_: la / submediant
- _M7_: ti / subtonic = leading tone

### Chromatic

- _P1_: do
- _m2_: di / ra
- _M2_: re
- _m3_: ri / me
- _M3_: mi
- _P4_: fa
- _TT_: fi / se
- _P5_: sol
- _m6_: si / le
- _M6_: la
- _m7_: li / te
- _M7_: ti

## Keys

- __tonic = root = base = keynote__: the note a scale is built on
- __signature__: a combination of sharps or flats, valid until the end of sheet;
  if there are sharps in a signature, there cannot be flats; if there are flats,
  there cannot be sharps
- __key__: a root note and a signature that define a scale that a piece of music
  is composed in
- __tonal centre__: the root note of a key; enharmonic with all chords in the
  given key
- __diatonic scale__: comprises of 7 notes; a combination of 5 tone and 2
  semitone intervals
- __chromatic scale__: comprises of all 11 diatonic steps
- __relative scales__: that have the same signature but different roots
- __parallel scales__: that have the same root note but different signatures
- __accidental__: a note that is not in the key of the most recent signature
- __circle of 5ths__: a 5th step in tonic adds one sharp note, a 4th step adds
  one flat note into the signature

### Signature identification

- _pattern of sharps in a signature_: F C G D A E B, clockwise movement in the
  circle of 5ths (mnemonic: *F*ather *C*hristmas *G*ets *D*ad *A*n *E*lectric
  *B*lanket)
- _pattern of flats in a signature_: B E A D G C F, anticlockwise movement in
  the circle of 5ths (mnemonic: *B*lanket *E*xplodes *A*nd *D*ad *G*ets *C*old
  *F*eet)
- _a sharp signature_: in major keys the last sharp of the signature is the
  leading note of the root
- _a flat signature_: in major keys the second-last flat of the signature is the
  root note itself; 1b is F major
- 5b has the same modified notes as 7#
- 7b has the same modified notes as 5#
- 6b has the same modified notes as 6#

### Signature calculation

#### Signatures to remember

- _F major_: 1b
- _C major_: no modified note
- _G major_: 1#
- _D major_: 2#
- _A major_: 3#
- _E major_: 4#
- _B major_: 5#

#### Calculations

- _numeric value of a sharp note in a signature_: +1
- _numeric value of a flat note in a signature_: -1
- _convert a key to its parallel sharp key_: add 7 to its numeric value
- _convert a key to its parallel flat key_: subtract 7 from its numeric value
- _convert a major key to its parallel minor_: subtract 3 from its numeric value
- _convert a minor key to its parallel major_: add 3 to its numeric value
- _modified notes in a signature_: all the unmodified notes in its parallel
  sharp or flat signature

### Key identification

#### In heard music

1. listen to the tune
2. hum the tonic; in tonal music it can be found by intuition
3. build major and minor scales on that note and see which one fits the tune

#### In written music

1. identify the signature; it defines 2 keys (a major & its relative minor)
2. look for the raised 7th; it implies harmonic minor scale
3. the bass note of the opening chord is usually the tonic
4. find authentic cadences and see if they are major or minor

### Interval identification

1. count the diatonic steps between the notes
2. build the major (Ionian) scale on the lower note
3. see whether the higher note fits the scale tone or it is sharp or flat

## Sight reading

### Notes

- _G clef lines_: E G B D F (mnemonic: *E*very *G*ood *B*oy *D*eserves *F*udge)
- _G clef spaces_: F A C E
- _F clef lines_: G B D F A (mnemonic: *G*rizzly *B*ears *D*on't *F*ear
  *A*nything)
- _F clef spaces_: A C E G (mnemonic: *A*ll *C*ows *E*at *G*rass)

### Reference notes

- __reference note__: a note in a stave that shall be immediately recognised;
  other notes shall be read by recognising the interval from the closest
  reference note
- the G and F clefs show the places of *G* and *F*
- the top line of G clef is *F*, the bottom line of F clef is *G*
- the 1st lower ledger line of G clef and the 1st upper ledger line of F clef
  are the *middle C* (the same note)
- the 2nd space from the top of G clef and the 2nd space from the bottom of F
  clef are *C*
- the 2nd upper ledger line of G clef and the 2nd lower ledger line of F clef
  are *C*

### Intervals

- _2nd_: adjacent line and space
- _3rd_: 2 adjacent lines or 2 adjacent spaces
- _4th_: one note is on a line, the other one is on a space, and there is 1 line
  between them
- _5th_: both notes are on a line and there is 1 line between them; or both are
  on a space and there is 1 space between them
- _6th_: one note is on a line, the other one is on a space, and there are 2
  lines between them
- _7th_: both notes are on a line and there are 2 lines between them; or both
  are on a space and there are 2 spaces between them
- _octave_: one note is on a line, the other one is on a space, and there are 3
  lines between them

### Middle C

- the 4th C on a regular piano keyboard
- the 3rd C on a 49-key keyboard
- 2nd string 3rd fret on guitar
- 4th string 5th fret on bass

## Scale building

### By intervals

#### Pentatonic scales

- __major pentatonic scale__: T T m3 T m3 / do re mi sol la do
- __minor pentatonic scale__: starts on 5 of major scale / do me fa sol te do
- __pygmy scale__: T s M3 m3 T / do re me sol te do

#### Hexatonic scales

- __whole tone scale__: T T T T T T / do re mi fi si li do
- __blues scale__: minor pentatonic + diminished 5

#### Heptatonic scales

- __diatonic scale__: a scale that includes 5 tone and 2 semitone steps
- __major scale__: T T s T T T s / do re mi fa sol la ti do

### Red-blue method

- __blue notes__: *C D E | F#(Gb) G#(Ab) A#(Bb)* (the 3 white keys from the C-E
  group + the 3 black keys from the F-B group)
- __red notes__: *Db(C#) Eb(D#) | F G A B* (the 2 black keys from the C-E group
  and the 4 white keys from the F-B group)
- each major scale is made up of 3 adjacent notes of the root note's colour and
  4 of the other colour moving upwards from the root
- each minor scale is made up of 2 adjacent notes of the root note's colour, 3
  of the other colour, and 2 of the first colour
- each letter should be represented in the scale (e.g. *F* is called *E#* in F#
  major)

## Diatonic scales

### Modal scales

- __Lydian__: sharp 4 / do re mi fi sol la ti do
- __Ionian / major__: do re mi fa sol la ti do
- __Mixolydian__: flat 7 / do re mi fa sol la te do
- __Dorian__:  flat 3, 7 / do re me fa sol la te do
- __Aeolian / natural minor__: flat 3, 6, 7 / do re me fa sol le te do
- __Phrygian__: flat 2, 3, 6, 7 / do ra me fa sol le te do
- __Locrian__: flat 2, 3, 5, 6, 7 / do ra me fa se le te do

### Minor scales

- __natural minor__: the Aeolian mode
- __harmonic minor__: flat 3, 6 / do re me fa sol le ti do
- __ascending melodic minor__: flat 3 / do re me fa sol la ti do
- __descending melodic minor__: the natural minor

### Jazz scales

- __Ionian b3__: the ascending melodic minor scale played both ascending and
  descending
- __Locrian b4__: flat 2, 3, 4, 5, 6, 7 / do ra me fe se le te do
- __Lydian b7__: sharp 4, flat 7 / do re mi fi sol la te do

### Ethnic scales

- __Arabic__: flat 2, 6 / do ra mi fa sol le ti do
- __Hungarian minor__: harmonic minor with sharp 4 / do re me fi sol le ti do

## Harmonic functions

### In major keys

- __tonic__: I iii vi / do mi la
- __subdominant__: ii IV / re fa
- __dominant__: V vii / so ti

### In minor keys

- __tonic__: i III / do me
- __subdominant__: ii iv VI / re fa le
- __dominant__: v VII / so te

### Roman numeral notation

- _upper case_: the harmony built on the scale degree has major 3rd
- _lower case_: the harmony built on the scale degree has minor 3rd

## Closely related keys

### To a major key

- _major IV, minor ii_: one less sharp or one more flat
- _major V, minor iii_: one more sharp or one less flat
- _minor vi (relative minor)_: same signature

### To a minor key

- _minor iv, major VI_: one less sharp or one more flat
- _minor v, major VII_: one more sharp or one less flat
- _major III (relative major)_: same signature

## Chord building

- __major__: P1 M3 P5 / do mi sol
- __minor__: P1 m3 P5 / do me sol
- __suspended 2nd [sus2]__: P1 M2 P5 / do re sol
- __suspended 4th [sus4]__: P1 P4 P5 / do fa sol
- __augmented [+]__: P1 M3 aug5 (major with aug5) / do mi si
- __diminished [o]__: P1 m3 dim5 (minor with dim5) / do me se
- __dominant 7th [7]__: major chord + m7 / do mi sol te
- __major 7th [M7]__: major chord + M7 / do mi sol ti
- __minor 7th [m7]__: minor chord + m7 / do me sol te
- __minor-major 7th [mM7]__: minor chord + M7 / do me sol ti
- __augmented 7th [+7]__: augmented chord + m7 / do mi si te
- __augmented-major 7th [+M7]__: augmented chord + M7 / do mi si ti
- __half-diminished 7th [ø7]__: diminished chord + m7 / do me se te
- __diminished 7th chord [o7]__: diminished chord + dim7 / do me se la
- __quartal chord__: P4 intervals / do fa te …

### Memorising chords

- the root and the 5th have the same accidental in all chords except for B and
  Bb triads
- the root and the 3rd have the same accidental in C, F, Gb, G major chords

## Chord inversions

- __chord inversion__: raising some of the notes of a chord by an octave
- __figured bass notation__: denoting chords in the sheet by the root note and
  numbers representing intervals from the root in descending order

### Triads

- _triad root position_ [none or 5/3]: I iii V (3rd, 5th from the root)
- _triad 1st inversion_ [6 or 6/3]: iii V I (3rd, 6th from the new root)
- _triad 2nd inversion_ [6/4]: V I iii (4th, 6th from the new root)
- _augmented chord_: its inversions are root position augmented chords with
  major 3rd root movement
- _diminished chord_: its inversions are root position diminished chords with
  minor 3rd root movement

### 7ths

- _7th chord root position_ [7]: I iii V vii (3rd, 5th, 7th from the root)
- _7th chord 1st inversion_ [6/5]: iii V vii I (3rd, 5th, 6th from the new root)
- _7th chord 2nd inversion_ [4/3]: V vii I iii (3rd, 4th, 6th from the new root)
- _7th chord 3rd inversion_ [2 or 4/2]: vii I iii V (2nd, 4th, 6th from the new
  root)

## Chords built on tonal degrees

### Triads

- _major scale_: I maj, ii min, iii min, IV maj, V maj, vi min, vii dim
- _natural minor scale_: i min, ii dim, III maj, iv min, v min, VI maj, VII maj
- _harmonic minor scale_: i min, ii dim, III aug, iv min, V maj, VI maj, vii dim

### 7th chords

- _major scale_: I maj7, ii min7, iii min7, IV maj7, V dom7, vi min7, vii
  half-dim7
- _natural minor scale_: i min7, ii half-dim7, III maj7, iv min7, v min7, VI
  maj7, vii dom7
- _harmonic minor scale_: i min-maj7, ii half-dim7, III aug-maj7, iv min7, V
  dom7, VI maj7, vii dim7

## Jazz improvisation

- __chordal improvisation__: the song structure is defined by a chord
  progression that prescribes a succession of fitting scales that the soloist
  follows
- __modal improvisation__: the song structure is definded by a succession of
  modes (modal scales) that both the soloist and the accompaniment follows
- __chord scale__: a scale that contains all chord notes, used to construct
  melodies over the chord
- __common scale__: a scale that is used to construct a melody over multiple
  successive chords

### Chord scales

- __over major chords__: Ionian, Lydian
- __over minor chords__: Dorian, Aeolian, Phrygian
- __over dominant 7th__: Mixolydian, Locrian b4, Lydian b7

### Finding fitting scales for a chord

- _major_: look for the 4th (natural → Ionian, sharp → Lydian)
- _minor_: look for the 6th first (natural → Dorian); the 2nd next (natural →
  Aeolian, flat → Phrygian)
- _dominant_: look for the 4th first (natural → Mixolydian, sharp → Lydian b7,
  flat → Locrian b4); any other flat notes next (→ Locrian b4)

### Selecting a scale for a chord

1. the chord symbol makes it clear, i.e. only one scale fits the chord notes
2. match the notes of an already written melody in the song
3. match the notes of the preceding chord, emphasising the harmonic motion

## Structure

- __motif / motive__: a short musical idea
- __phrase__: a group of motives, punctuated with a cadence
- __cadence__: a chord progression that closes a phrase
- __ostinato / loop__: repetitions of a motif or phrase at the same pitch
- __period__: a couple of connected phrases, the 1st has half cadence while the
  2nd has authentic cadence
- __standard period / sentence__: an 8-bar-long period
- __antecedent phrase / question__: the 1st phrase of a period, concluded with a
  half cadence
- __consequent phrase / answer__: the 2nd phrase of a period, concluded with an
  authentic cadence
- __section__: a group of phrases and/or periods
- __double bar__: symbol that indicates the borders of sections in the sheet
- __turnaround__: a cadence that connects a section with its repetition
- __movement / song__: a group of sections
- __theme / subject__: a phrase that is the main musical idea of a section in a
  stable key with memorable melody and/or rhythm
- __transition / bridge__: a phrase that links themes
- __closing__: a phrase that is harmonically stable and implies a sense of
  completeness
- __coda / outro__: a section that consists of one prolonged cadence that brings
  a movement to an end

### Repetition

- __da capo__: repeat from the beginning
- __dal segno__: repeat from the sign
- __al coda__: repeat until the coda section
- __al fine__: repeat, then play until the end

## Cadences

- __perfect authentic cadence__: V[6/4] - I[6], the highest voice is the leading
  tone resolving to the tonic
- __imperfect authentic cadence__: V - I[6/4] or V[6] - I, the leading tone
  resolves to tonic in lower voices
- __plagal cadence / amen cadence__: IV - I[6]
- __double plagal cadence__: IV - I - II, two modulating plagal cadences, I of
  the original key is IV in the key of II
- __half cadence__: ends on V, typically I - V or IV - V
- __deceptive / interrupted cadence__: ends on any other degree, typically V -
  IV or V - ii or V - vi
- __cadential six-four__: I[6/4] - I or I[6/4] - V - I
- __jazz turnaround__: ii - V - I
- __blues turnaround__: V - IV - I

## Harmonising

- __modulation__: changing the key in the middle of a tune
- __diatonic substitution__: swapping a chord for one that has the same harmonic
  function in the same key
- __modal interchange__: swapping a chord for one on the same degree in a
  parallel mode
- __flat five substitution__: swapping a dominant chord for the dominant chord
  a tritone above
- __neapolitan substitution__: swapping the ii or IV chord for the first
  inversion of the flat ii
- __secondary dominant__: dominant chord a perfect fifth above a tonal degree
- __tonicisation__: resolving to a major or minor chord from its secondary
  dominant
- __constant structure__: playing the same type of chord successively on
  different scale degrees
- __circle of 5ths progression__: root movement subsequently by 5ths upwards or
  4ths downwards
- __pivot chord__: that exists in two keys and creates smooth transition while
  modulating between those two keys
- __closed structure__: arrangement of chord notes so that they are the closest
  notes
- __open structure__: arrangement of chord notes so that they are more than an
  octave apart

## Melodising

- __voicing__: how notes of a chord are ordered and spaced
- __block chord__: played as a sound shape, all notes at the same time
- __broken chord__: chord notes are played one-by-one creating a melody line
- __arpeggio__: chord notes are played in succession upwards or downwards
- __dominant approach__: resolving to a chord note from a perfect fifth above or
  below
- __chromatic approach__: resolving to a chord note from a semitone above or
  below
- __scale approach__: resolving to a chord note from a diatonic scale step above
  or below
- __anticipation__: connecting two chords with a note of the second chord
- __scale-wise motion__: connecting chord notes with diatonic scale tones that
  occur in between
- __chromatic motion__: connecting chord notes with chromatic scale tones that
  occur in between
- __guide tone__: a note of the chord that defines the quality of the chord,
  usually the 3rd or the 7th
- __guide tone line__: melody line following the guide tones of successive
  chords to outline the harmonic motion
- __cantus firmus__: composition method in which a guide tone line is used as a
  draft that outlines the melody

## Voice leading

- __step / conjunct motion__: movement by a semitone or tone interval
- __leap / skip / disjunct motion__: movement by an interval larger than a tone
- __imitation__: repetitions of a motif or phrase at different voices
- __variation__: imitation with alterations instead of exact repetition
- __transposition__: variation by starting the phrase on a different pitch
- __real transposition__: all intervals are exactly maintained
- __tonal transposition__: transposing to another tonal degree in the same key;
  the quality of the intervals may change according to diatonic relationships
- __inversion__: variation by inverting each interval in a phrase
- __intervallic change__: variation by expanding or contracting intervals
- __augmentation__: variation with longer notes than previously used
- __diminution__: variation with shorter notes than previously used
- __addition / embellishment__: new notes are added to a phrase at each
  repetition

## Texture types

- __monophonic / unison music__: that consists of a single melody line
- __homophonic / chordal music__: that consists of block chords; all voices move
  in parallel motion
- __polyphonic / contrapuntal music__: that consists of multiple melody lines

## Polyphony

- __countermelody__: a second melody line that supports the leading voice
- __accompaniment__: block chords that support the leading voice
- __canon__: multiple voices follow each other, each one imitating the leading
  phrase
- __heterophony__: multiple voices play variations of a phrase simultaneously
- __antiphon__: multiple voices are distributed in space (at different parts of
  a venue)
- __parallel motion__: two melodic parts move so that they keep the same
  interval
- __similar motion__: two melodic parts move in the same direction
- __oblique motion__: one melodic part remains static while the other moves
- __contrary motion__: one melodic part rises while the other falls

## Bass lines

- __ground bass / riff / basso ostinato__: a steadily repeated bass phrase
- __drone__: a continuously sounded note through an entire phrase
- __pedal point__: a sustained bass note (drone) against a changing harmony
- __line cliché__: a stepwise moving bass line against a sustained chord
- __walking bass__: bass line with steady quarter note movement
- __Alberti bass__: broken chord bass line pattern I - V - III - V as 8th notes
- __disco bass__: bass line with 8th note octave leaps up and back

## Composition rules

- balance of _uniformity_ (keeping the music intelligible) and _variety_
  (keeping the music interesting)

### Harmony

- use inversions and substitutions to keep the same notes when changing from one
  chord to the next
- the tonic chord should most of the time be in root position
- _root movement by 2nd_: subsequent triads have no notes in common → rough
- _root movement by 3rd_: subsequent triads have two notes in common → smooth
- _root movement by 4th_: subsequent triads have one note in common → balanced

### Melody

- leaps up in a melody should be followed by steps down
- leaps down in a melody should be followed by steps up
- when moving from one note to the next, move to the next closest one (i.e.
  avoid leaps larger than an octave in a melody)

### Counterpoint

- avoid tritone and minor 9th between voices
- avoid gaps larger than an octave between adjacent voices (except for the bass
  line that may be further apart)
- avoid voice crossing, i.e. when a voice gets higher or lower than the melody
  in an adjacent voice
- when two voices move in the same direction, avoid consecutive fifths and
  octaves

## Classical form

- __prime__: a section that is the same as an earlier section with some
  modifications; e.g. A → A'
- __binary form__: a movement that comprises of 2 sections, A and B (A modulates
  to B)
- __ternary form__: a movement with 3 sections in the form A B A'
- __fugue__: a movement in ternary form; each section is built on imitations of
  two themes called the subject and the countersubject
- __sonata form / first movement form__: a movement in ternary form; the 3
  sections are the exposition, development and recapitulation
- __exposition [sonata]__: opening section with 2 contrasting themes, the 2nd
  theme is in a different key (usually the dominant or the relative major)
- __development [sonata]__: intermediate section that varies themes from the
  exposition through modulations to different keys, closed with a retransition
- __retransition [sonata]__: a prolonged harmony (usually the dominant 7th) that
  leads back to the main theme
- __recapitulation [sonata]__: final section that repeats the themes of the
  exposition but stays in the tonic key, closed with a coda
- __large ternary form / second movement form__: a movement in ternary form
  where section B is in a major key if section A is minor or vice versa
- __rondo form / third movement form__: a movement with a returning theme in the
  form A B A C A D A …

## Classical styles

- __opus / work__: a piece of music
- __concerto__: a piece in which a solo instrument is accompanied by an
  orchestra
- __cadenza__: a solo section in a concerto in which only the soloist plays and
  the orchestra remains silent
- __symphony__: an orchestral piece that comprises of 3 or 4 movements, usually
  allegro, adagio, (minuet or scherzo,) rondo
- __minuet__: a French dance style in 3/4
- __scherzo__: musical "joke", a fast and playful movement
- __rondo__: an allegro movement in the rondo from
- __overture__: a piece that comprises of a single movement, to start an opera
  or concert with
- __programme music__: a musical piece that tells a story
- __character piece__: a musical piece that portrays a scene
- __absolute music__: music without non-musical references
- __reduction__: transcription of an orchestral piece for a solo instrument
- __atonal music__: music without an identifiable scale and tonal centre
- __serialism__: a composition technique for creating atonal music that gives
  each note of a scale equal priority
- __dodecaphony / 12-tone serialism__: a phrase contains all 12 notes of the
  chromatic scale in a succession

## Symphony orchestra

- __woodwinds__: flutes, oboes, clarinets, bassoons
- __brass__: French horns, trumpets, trombones, tubas
- __percussion__: timpani, snare, bass drum, cymbals, triangle
- __keyboards__: piano, celesta
- __strings__: harps, violins 1, violins 2, violas, cellos, double basses

## Vocal music

- __aria__: vocal solo
- __recitative__: a song in which singing resembles speech
- __lieder__: a poem performed by a singer, accompanied by a piano
- __a capella__: music for voices with no accompaniment
- __libretto__: all text associated with an opera or musical
- __lyrics__: the words a singer sings
- __chest voice__: vocal register in which air feels to be resonating in the
  chest
- __head voice__: vocal register in which air feels to be resonating in the head
- __middle/mixed voice__: transition register between head voice and chest voice
- __vocal break / passaggio__: the boundary between two vocal registers

## Jazz

- __head__: a composed melody that a jazz tune starts with and improvised solos
  are based on; usually 32 bars long
- __combo__: small orchestra
- __big band__: large orchestra with 4 sections: saxophones, trumpets,
  trombones, rhythm section
- __comping (piano, guitar)__: accompanying an improvised solo responsively with
  chords and counter-melodies
- __comping (drumset)__: complementing an improvised melody responsively on the
  drums, while keeping time on the cymbals

## Percussion

- __fulcrum__: the grip on the drumstick by the thumb and index fingers
- __rudiment__: a simple rhythm pattern that more complex patterns are built
  from
- __stroke__: a note performed on a drum
- __full stroke__: a stroke from high hand position to high hand position
- __down stroke__: a stroke from high hand position to low hand position
- __up stroke__: a stroke from low hand position to high hand position
- __tap__: a stroke from low hand postition to low hand position
- __double stroke__: two successive strokes played by the same hand
- __diddle__: a double stroke played at the speed of the context
- __paradiddle__: two single strokes followed by a double stroke played at the
  speed of the context
- __drag__: a double stroke played at twice the speed of the context
- __flam__: two single strokes, the first is a grace note and the second is an
  accented note
- __roll__: sustained sound by allowing the stick to bounce multiple times after
  a stroke
- __single stroke roll__: roll with 1 stroke per hand alternating between left
  and right
- __open / double stroke roll__: roll with 2 strokes per hand alternating
  between left and right
- __closed / multiple stroke / buzz roll__: roll with multiple short notes per
  hand alternating

### Grip types

- __traditional grip__: the left grip is _overhand_, the right grip is
  _underhand_
- __matched grip__: the two hands mirror each other
- __German grip__: palms are horizontal, drumsticks are perpendicular
- __French grip__: palms are vertical, drumsticks are parallel
- __American grip__: palms are at 45° to the ground, drumsticks are at 45°

## String instruments

- __fretting__: pressing a string against a fret at a specific interval on
  fretted instruments
- __stopping__: pressing a string against the fingerboard at a specific interval
  on fretless instruments
- __position__: an alignment of fingers on the fingerboard
- __double stop__: playing two notes simultaneously on a bowed instrument

## Guitar

- __guitar position__: a number that identifies the fret that the first (index)
  finger is aligned to
- __tirando / free stroke__: plucking a string by pulling it towards the body
- __apoyando / rest stroke__: plucking a string by pulling it upwards and
  resting the finger on the string above
- __picado__: rest stroke where the note is immediately silenced after plucking
  the string, used in flamenco
- __slur__: playing multiple notes with a single pluck of a string
- __hammer-on__: slur upwards
- __pull-off__: slur downwards
- __tremolo__: plucking a string rapidly and monotonously, which results in a
  drone-like sound
- __rasgueado__: rhythmic strumming of block chords with one finger at a time,
  mostly used in flamenco
- __sul ponticello__: instruction to pluck the strings near the bridge
- __sul tasto__: instruction to pluck the strings over the fingerboard
- __scordatura__: non-standard tuning (e.g. dropped D)

### Finger notation

- left hand: _1 - 4_ for index, middle, ring and pinky fingers
- right hand: _p, a, m, i_ for thumb (pulgar), index (indico), middle (medio)
  and ring (anular) fingers

## Piano

### Finger notation

- _1 - 5_ for thumb, index, middle, ring and pinky fingers
- avoid using the thumb for black keys if possible

### Pedals

- __sustain / damper pedal__: lifts all string dampers up, sustaining any note
  after their keys are released
- __soft pedal__: reduces the number of strings being hit by a hammer from 3 to
  2 or 1
- __sostenuto pedal__: keeps the string dampers lifted of the keys being pressed
  at the time the pedal is pressed, sustaining those notes but not sustaining
  any other note

### Pedaling techniques (sustain pedal)

- __delayed pedal__: pressed immediately after playing the first note of a
  harmony and released just before the harmony changes
- __simultaneous pedal__: pressed and released at the same time as a key of the
  piano, to add emphasis to certain notes
- __preliminary pedal__: pressed before playing a note, to add a dramatic effect
  to an opening chord
