# Sound Engineering Notes

## Three dimensions of a sound in engineering [position on the soundstage]

- Dynamic range [front to back]
- Frequency content [top to bottom]
- Stereo pan position [left to right]

## Three dimensions of a sound in music

- Pitch
- Duration
- Dynamics

## Sound engineering process

1. Tracking
2. Mixing
3. Mastering

## Analysis tools

- Oscilloscope / waveform meter
- Spectrum analyser
- Sonogram / spectrogram
- Lissajous meter

## Psychoacoustic effects

- Haas effect
- Equal-loudness (Fletcher-Munson) curves
- Auditory masking

## Sound engineering compromises

- __Gain staging__: setting gain and dynamic range at each stage of the signal
  chain high enough for good SNR but low enough to avoid distortion
- __Frequency shaping__: identifying and filtering/panning conflicting (masking)
  frequencies between tracks
- __Mixing__: due to the uneven frequency response of the human ear the
  perceived levels are different at high volume and low volume
- __Limiting/compression__: increasing the overall loudness and punchiness of a
  mix without adding unwanted harmonics and flattening out the dynamics

## Terms

- __Q__ = dB/octave
- __ADSR__ = Attack time, Decay time, Sustain level, Release time
- __ARRT__ = Attack time, Release time, Ratio, Threshold level

## Compressor setup

1. Attack: start at minimum
2. Release: start at minimum
3. Ratio: start at maximum
4. Threshold: start at an extreme setting (around -20 dB)
