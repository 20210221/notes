# Generative Testing Reading List

## Testing Methodologies

### Fault injection

- [Fault injection](https://en.wikipedia.org/wiki/Fault_injection)
- [Fault Injection in Production](https://queue.acm.org/detail.cfm?id=2353017)
  (case study)

### Fuzzing

- [Fuzzing](https://en.wikipedia.org/wiki/Fuzzing)
- [American Fuzzy Lop White Paper](http://lcamtuf.coredump.cx/afl/technical_details.txt)
- [The Fuzzing Book](https://www.fuzzingbook.org/)

### Model-based testing

- [Model-based Software Testing](http://www.testoptimal.com/ref/Model-based%20Software%20Testing.pdf)
  (overview)
- [Model-based Testing at Spotify](https://www.youtube.com/watch?v=psOThLDKOFc)
  (talk)

### Property-based testing

- [Experiences with QuickCheck: Testing the Hard Stuff and Staying Sane](https://www.cs.tufts.edu/~nr/cs257/archive/john-hughes/quviq-testing.pdf)
- [Reliable Systems Series: Model-Based Property Testing](https://medium.com/@tylerneely/reliable-systems-series-model-based-property-testing-e89a433b360)
- [PropEr Testing Book](https://propertesting.com/toc.html)

### Formal test derivation methods

- [Testing Derivation](http://users.encs.concordia.ca/home/b/bentahar/INSE6260/Lectures/Week7-6260%202016.pdf)
  (lecture slides)
- [Formal Methods for Protocol Testing: A Detailed Study](https://www.csee.umbc.edu/courses/graduate/681/Fall07/formal_methods_protocol_testing.pdf)
- [Testing Software Design Modeled by Finite-State Machines](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.475.3576&rep=rep1&type=pdf)

### Observation-based modeling

- [Observation-based Modeling for Model-based Testing](https://www.esi.nl/publications/poseidon/Observation_based_Modeling_for_Model_based_Testing.pdf)
- [Model Generation to support Model-based Testing Applied on NASA DAT](https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20180004562.pdf)
  (case study)

## Modeling Tools

### Statecharts

- [The World of Statecharts](https://statecharts.github.io/)
- [Statecharts: A Visual Formalism for Complex Systems](https://www.sciencedirect.com/science/article/pii/0167642387900359)

### Markov chains

- [Introduction to Markov Chains](https://towardsdatascience.com/introduction-to-markov-chains-50da3645a50d)
- [Using Markov Chains to Generate Test Input](https://electric-cloud.com/blog/using-markov-chains-to-generate-test-input/)
- [Software Reliability Test Based on Markov Usage Model](http://www.jsoftware.us/vol7/jsw0709-18.pdf)

### Petri nets

- [What is a Petri Net?](https://www-dssz.informatik.tu-cottbus.de/publications/materials/desel_2001_what_is_a_pn.pdf)
- [Petri Nets](http://www.inf.ed.ac.uk/teaching/courses/mlcsb/2007-2008/references/Peterson_Petri_net_survey.pdf)

### Timed automata

- [Timed automaton](https://en.wikipedia.org/wiki/Timed_automaton)
- [A Theory of Timed Automata](https://www.sciencedirect.com/science/article/pii/0304397594900108)
  (formal)

### Input/output automata

- [Input/output automaton](https://en.wikipedia.org/wiki/Input/output_automaton)
- [An Introduction to Input/Output Automata](https://groups.csail.mit.edu/tds/papers/Lynch/CWI89.pdf)
  (formal)
