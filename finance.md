# Finance Notes

## Money

- __commodity money__: money with intrinsic value, e.g. gold
- __fiat money__: money without intrinsic value, its value is guaranteed by a
  governing body
- __currency__: money as physical notes and coins representing legal claims
  against the central bank, issued by the central bank, available to everyone
- __deposit__: money as the balance of a digital account representing a legal
  claim against a commercial bank, issued by the commercial bank with 1:1
  exchange value with a currency, available to customers of the commercial bank
- __reserve__: money as the balance of a digital account that a _clearing bank_
  holds at the central bank, representing a legal claim against the central
  bank, issued by the central bank, available to selected banks and financial
  institutions
- __CBDC (Central Bank Digital Currency) / e-money__: money as digital coins
  representing legal claims agains the central bank, issued by the central bank,
  available to everyone
- __broad money__: instant access money (currencies and bank deposits)
- __narrow money__: claims agains the central bank (currencies and reserves)
- __clearing bank__: a bank that has an account with the central bank, therefore
  it can borrow central bank reserves and send money to other clearing banks
  without holding accounts at those banks
- __seigniorage__: the difference between the face value of money and the cost
  to produce it

## Corporate Finance

- __assets__: the total value of cash, property, etc. that a company owns
- __liabilities__: the total value of debt, interest, etc. that a company owes
  to someone
- __equity__: assets minus liabilities; the book worth of a company
- __market cap__: the sum of the value of all shares of a publicly traded
  company; the market worth of the company
- __affiliate__: a company whose voting stock is less than 50% controlled
  by another company
- __subsidiary__: a company whose voting stock is more than 50% controlled
  by another company,  referred to as the _parent company_
- __capitalisation__: the sum of a corporation's stock, long-term debt and
  retained earnings
- __market capitalisation__: a company's outstanding shares multiplied by their
  share price
- __gearing__: the percentage of a company's level of long-term debt compared
  to its equity capital
- __solvency__: the ability of a company to meet its long-term fixed expenses
- __insolvency__: when a company can no longer meet its financial obligations
  with its lenders as debts become due
- __operating profit__: the profit earned from a firm's normal core business
  operations, without any profit earned from the firm's investments and the
  effects of taxes
- __Return on Assets (ROA) / Return on Invesment (ROI)__: an indicator of how
  profitable a company is relative to its assets; calculated by dividing a
  company's annual earnings by its total assets
- __Return on Equity (ROE)__: measures a corporation's profitability by
  revealing how much profit a company generates with the money shareholders have
  invested; calculated by dividing a company’s annual net income with the
  shareholders’ equity
- __environmental, social and governance (ESG) criteria__: a framework for
  rating corporations based on their sustainability record to drive ethical
  investment decisions

## Investments

- __asset__: a resource with economic value
- __fungible asset__: whose instances are interchangeable (e.g. money, vote,
  stake)
- __non-fungible asset__: whose instances are unique (e.g. real estate,
  collectibles)
- __instrument__: a tradeable asset
- __liquidity__: the ability of an asset to be converted to cash quickly
- __security__: a financial contract with monetary value, used as an instrument
- __bond__: a security that represents a fraction of a loan to a company
- __stock__: a security that represents a fraction of ownership of a company
- __securitisation__: the process through which an issuer creates a new
  instrument by combining other assets
- __yield__: the return on an investment, i.e. the interest or dividends
  received from a security
- __leverage__: the use of debt (borrowed capital) in order to undertake an
  investment or project
- __hedge__: an investment to reduce the risk of adverse movements in the price
  of an asset; analogous to taking out an insurance policy
- __derivative__: a security that derives its price from fluctuations in the
  price of an underlying asset, used for hedging
- __face value__: the original price of a security as set by its issuer
- __principal__: the original sum of money borrowed in a loan or put into an
  investment, separate from earnings and interest
- __redemption__: the return of an investor's principal
- __payoff__: the profit or loss from the sale of an instrument after the costs
  have been subtracted
- __maturity__: the date on which an instrument expires, e.g. the day the
  outstanding principal of a bond is repaid
- __collateral__: an asset that a lender accepts as security for a loan; if the
  borrower defaults on the loan payments, the lender can seize the collateral
  and resell it to recoup the losses
- __escrow__: the use of a third party who holds an asset before they are
  transferred from one party to another; the third party holds the funds until
  both parties have fulfilled their contractual requirements
- __clearing house__: an intermediary between a buyer and seller to ensure that
  the process from trade inception to settlement is smooth
- __position__: the amount of a security, commodity or currency that someone
  owns; they come in two types: _long positions_ and _short positions_
- __long position__ / __going long__: a security is bought and held,
  anticipating an price increase; to be sold at a future price (i.e. buy low
  then sell high)
- __short position__ / __going short__: a security is borrowed and sold,
  anticipating a price decrease; the borrower will then have to buy at a future
  price in order to return the borrowed security (i.e. sell high then buy low)
- __bullish market__: a market situation in which most traders are going long
- __bearish market__: a market situation in which most traders are going short
- __forward__: a financial contract obligating a buyer to buy an asset or a
  seller to sell an asset at a predetermined future date and price
- __future__: a special forward contract that is traded on an exchange; a
  clearing house guarantees that the transaction will take place at maturity
- __option__: a financial contract that gives the owner the right, but not the
  obligation, to buy an asset (_call option_) or to sell and asset (_put
  option_) at a predetermined future date and price
- __exercising__: activating an option, fulfilling the option contract
- __American option__: the owner of the option can exercise the option any time
  after buying it until the time of expiration
- __European option__: the owner of the option can only exercise it at the time
  of expiration
- __grant / strike price__: the price at which the underlying security can be
  purchased or sold when exercising an option
- __vesting__: acquiring the right to use an option (that has been given in a
  company incentive scheme)
- __spot price__: the price buyers and sellers agree for the transfer of one
  unit of an asset immediately
- __notional value__: the total value of a position as the number of units in
  the contract multiplied by the spot price
- __margin__: debt or borrowed money a firm uses to invest in other financial
  instruments; used to create leverage
- __margin account__: an account with a brokerage firm that allows an investor
  to buy securities with cash loaned by the broker; there must be enough money
  in the account to cover any losses which may occur
- __initial margin__: the percentage of the purchase price of a security
  (purchased _on margin_, i.e. from a margin account) that an account holder
  must pay for with available cash in the margin account
- __maintenance margin__: a fraction of the initial margin that has to be
  available in the trader's account to keep the trade open; if the trader's
  account balance drops below this threshold then the account receives a _margin
  call_ and has to be replenished to satisfy initial margin requirements 
- __variation margin__: the amount of money (as a percentage of the trade
  notional) to be posted in the margin account to return its balance to the
  initial margin level after the maintenance margin has been breached
- __mark to market__: the practice that the value of a futures contract is
  periodically adjusted to follow the current market price; the trading partners
  compensate each other for the gains and losses through payments between their
  margin accounts
- __contango__: a market situation in which the current spot price of an asset
  is lower than the prices of its futures
- __backwardation__: a market situation in which the current spot price of an
  asset is higher than the prices of its futures
- __market maker__ / __liquidity provider__: a broker who offers to take either
  side in a bilateral trade (i.e. willing to either buy or sell)
