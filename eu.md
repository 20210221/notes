# Institutions of the EU

## European Parliament (EP)

- Legislative body
- Directly elected
- HQ in Brussels & Strasbourg
- Has a president, elected by members of the EP for half term (David Sassoli)

## European Council (EC)

- Governing body
- Presidents and PMs of the member states
- Has a president, elected by members of the EC (Charles Michel)
- Regular summits held at various locations

## Council of the EU / Council of Ministers

- Legislative body
- Government members (ministers) of the member states
- A member state holds presidency for 6 months

## European Commission

- Governing body
- One commissioner per member state proposed by the government of the member
  state, approved by the EP
- Has a president, proposed by the EC, elected by the EP (Ursula von der Leyen)
- HQ in Brussels

## Court of Auditors

- Investigatory agency
- One auditor per member state proposed by the government of the member state,
  approved by the Council of Ministers
- HQ in Luxembourg

## European Court of Justice

- Judiciary body
- HQ in Luxembourg

## European Central Bank

- Central bank for the Euro
- HQ in Frankfurt

## Agencies

- Policy and research organisations
- Each member state is hosting 1–3 agencies

## (International Court of Justice)

- UN institution, independent from the EU
- HQ in The Hague
