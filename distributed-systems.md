# Distributed Systems Reading List

## Basics

* [Byzantine fault](https://en.wikipedia.org/wiki/Byzantine_fault)
* [State machine replication](https://en.wikipedia.org/wiki/State_machine_replication)
* [CAP theorem](https://en.wikipedia.org/wiki/CAP_theorem)
* [Consistency Models](http://jepsen.io/consistency)
* [FLP Impossibility](https://www.the-paper-trail.org/post/2008-08-13-a-brief-tour-of-flp-impossibility/)
* [Bounds on fault tolerance](http://groups.csail.mit.edu/tds/papers/Lynch/jacm88.pdf)
* [Distributed-systems-readings](https://henryr.github.io/distributed-systems-readings/)
* [Distributed systems for fun and profit](http://book.mixu.net/distsys/single-page.html)

## Consensus

* [Practical Byzantine Fault Tolerance (1999)](http://pmg.csail.mit.edu/papers/osdi99.pdf)
* [The latest gossip on BFT consensus (2018) - v3](https://arxiv.org/abs/1807.04938)
* [Tendermint Explained: Bringing BFT-based PoS to the Public Blockchain Domain](https://blog.cosmos.network/tendermint-explained-bringing-bft-based-pos-to-the-public-blockchain-domain-f22e274a0fdb)
* [Tendermint: Byzantine Fault Tolerance in the Age of Blockchains](https://atrium.lib.uoguelph.ca/xmlui/bitstream/handle/10214/9769/Buchman_Ethan_201606_MAsc.pdf?sequence=7&isAllowed=y)

## Testing

* [Measuring Correctness of State in a Distributed System](https://blog.wallaroolabs.com/2017/10/measuring-correctness-of-state-in-a-distributed-system/)
* [Testing Distributed Systems for Linearizability](https://www.anishathalye.com/2017/06/04/testing-distributed-systems-for-linearizability/)
* [List of resources on testing distributed systems](https://github.com/asatarin/testing-distributed-systems)

## Intro talks/videos

* [The Secret Lives of Data](http://thesecretlivesofdata.com/raft/) (visualisation)
* [Intro to the Cosmos Network with Sunny Aggarwal](https://www.youtube.com/watch?v=XAetXKTikLM)
* [Cosmos Proof of Stake with Sunny Aggrawal](https://www.youtube.com/watch?v=XxZ04w2x4nk)
* [A network partition survival guide](https://www.youtube.com/watch?v=uTJvMRR40Ag&t=136)
