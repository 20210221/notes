######################
# Ruby Peculiarities #
######################

# RDoc considers any comment right above a function definition as
# documentation comment; use the `ri` command to read it
def do_something
end

# Literal string interpolation is supported, double quotes only (like Bash)
name = 'Joe'
raise unless "Hi #{name}" == 'Hi Joe'
raise unless 'Hi #{name}' == "Hi \#{name}"

# Hashes have an alternative syntax if all keys are symbols
raise unless {:one => 1, :two => 2} == {one: 1, two: 2}

# The '!' postfix denotes in-place operation,
# the '?' postfix denotes a predicate
x = "foobar"
x.sub!("foo", "bar")
raise unless x == "barbar"
raise unless x.is_a? String

#   ___________
# _| Functions |_______________________________________________________________

# Operators are syntactic sugars for methods
raise unless 2 + 1 == 2.+(1)
# As a result chaining operators is not possible
# 1 != 2 != 3 -> syntax error

# Functions return the value of the last expression
def one
    1
end

# Or use explicit return
def two
    return 2
end

# Brackets are optional
raise unless one == 1 and two() == 2

#   ___________
# _| Constants |_______________________________________________________________

THIS = 'me'  # Uppercase name: constant
THIS = 'you'  # Will print a warning
THIS.sub!('you', 'him')  # This is allowed

this = 'me'.freeze  # `freeze`: Make immutable
begin
  this.sub!('me', 'you')
  raise
rescue RuntimeError => e
  raise unless e.message == "can't modify frozen String"
end

#   _________________
# _| Variable scopes |_________________________________________________________

$stop = '!'  # `$`: Global variable

class Greeter
  @@greeting = 'Hi'  # `@@`: Class variable

  def initialize(name)  # `initialize`: The constructor
    @name = name  # `@`: Instance variable
  end
end

# Properties are always private
raise if Greeter.new('Joe').respond_to? 'name'
raise if Greeter.respond_to? 'greeting'

# Modules and classes are open for extension, including built-ins
class Greeter  # This is the same class as the one above!
  def greet  # Instance method
    "#{@@greeting} #{@name}#{$stop}"
  end

  def self.greeting  # `self.`: Class method
    @@greeting
  end

  def name  # Getter
    @name
  end

  def name=(value)  # Setter
    @name = value
  end

  attr_accessor :name  # Auto-generates the two methods above
end

joe_greeter = Greeter.new("Joe")
raise unless Greeter.greeting == 'Hi'
raise unless joe_greeter.greet == 'Hi Joe!'

# Instances don't allow access to class methods and vice versa
raise if joe_greeter.respond_to? 'greeting'
raise if Greeter.respond_to? 'greet'

#   __________
# _| Closures |________________________________________________________________

# 1st type: Blocks

class Array  # This is the built-in `Array` class
  # `&block` is a reference to a block of code sent into the function
  def map_v1(&block)
    arr = []
    self.each do |n|
      arr.push(block.call(n))
    end
    arr
  end

  def map_v2
    arr = []
    self.each do |n|
      # `yield` calls the block sent into the function
      arr.push(yield(n))
    end
    arr
  end
end

# Single-line block
raise unless [1, 2, 3].map_v1 { |x| x ** 2 } == [1, 4, 9]

# Multiline block
raise unless ([1, 2, 3].map_v2 do |x|
  x ** 2
end) == [1, 4, 9]

# 2nd type: Procs

class Array
  # `code` should be a `Proc` instance
  def map_v3(code)
    arr = []
    self.each do |n|
      arr.push(code.call(n))
    end
    arr
  end
end

raise unless [1, 2, 3].map_v3(Proc.new { |x| x ** 2 }) == [1, 4, 9]

# 3rd type: Lambdas

# `lambda` keyword
raise unless [1, 2, 3].map_v3(lambda { |x| x ** 2 }) == [1, 4, 9]

# `->`: Short notation
raise unless [1, 2, 3].map_v3(-> (x) { x ** 2 }) == [1, 4, 9]

# Under the hood blocks and lambdas are special types of procs
raise unless (-> () {}).class == Proc

# Blocks and procs are not allowed to have return statements, but lambdas are
def generic_call(code)
  code.call
end

raise unless generic_call(lambda { return :lambda }) == :lambda

begin
  generic_call(Proc.new { return :Proc })
  raise
rescue LocalJumpError => e
  raise unless e.message == 'unexpected return'
end

#   _______________
# _| Import system |___________________________________________________________

# `require`: Load all modules and classes from a file
raise if defined? JSON
require 'json'
raise unless defined? JSON

# `module`: Define a namespace
module A
  def get_me
    @me
  end
end

# Mixins: preferred over inheritance

# `include`: Import the module's functions into the class as instance methods
class B
  include A

  def initialize
    @me = 'B'
  end
end

raise if B.respond_to? 'get_me'
raise unless B.new.get_me == 'B'

# `extend`: Import the module's functions into the class as class methods
class C
  extend A

  def initialize
    @me = 'C'
  end
end

raise unless C.get_me == nil
raise if C.new.respond_to? 'get_me'
