# Writing Notes

## Tools

- __Morning pages__: write 3 pages on paper every morning about whatever crosses
  your mind, to kickstart your daily routine of writing (Julia Cameron)
- __Artist dates__: go on a solo exploration once a week and take notes, to
  boost creativity (Julia Cameron)
- Write a story __3 times__: the 1st time to understand it, the 2nd time to
  improve the prose, the 3rd time to make it compelling (Bernard Malamud)

## Voice

- _Simplicity_: makes writing punchy (e.g. active voice over passive)
- _Clarity_: makes writing easy to understand
- _Elegance_ (rhythm and structure): makes writing flow well
- _Evocativeness_ (imagery): makes writing stimulating

## Style

- _Classic_: conversation between the writer and the reader through a shared
  vision of reality
- _Practical_: transparent; draws attention away from the writing to the subject
- _Reflexive_: writing about the writing itself (e.g. signposting; commenting on
  the reliability of a source)
- _Rhetorical_: conveys a single viewpoint

## Structure

- Introduction
    - Background
    - Thesis statement
    - Summary of the outline
- Main body
    - Essays
        - Argument
        - Possible objections
        - Replies to the objections
    - Narratives
        - Setup
        - Conflict
        - Resolution
- Conclusion
    - Restate the thesis
    - Review
    - Comment

## Process

- __Discovery__: 1st stage; for yourself
    - Brainstorm
    - Research
    - Outline: via headings and sub-headings
    - Vomit draft: a quick and dirty first draft
- __Presentation__: 2nd stage; for the intended audience
    - Draft
    - Rewrite (multiple times)
    - Add references

## Strategies to overcome block

- Create a task list
- Write from the middle out
- Write in the form of a dialogue or conversation
- Write in the voice of a favourite author

## Rules of storytelling

- Skip introductory remarks, dive into the story instead
- The story should revolve around a strong conflict
- Show the change in the character as a result of the conflict
- Provide specific details (e.g. about the characters' appearance)
- Show, don't tell using sensory details (VAKOG)
    - Visual (seeing)
    - Auditory (hearing)
    - Kinesthetic (feeling)
    - Olfactory (smelling)
    - Gustatory (tasting)
- Use dialogue, not narration
- Wrap up with a takeaway message
