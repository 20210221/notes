# Ethereum Reading List

## Introduction

- [A Gentle Introduction to Blockchain Technology](https://bitsonblocks.net/2015/09/09/gentle-introduction-blockchain-technology/)
- Everything You Wanted To Know About Blockchains [Pt.1](https://unwttng.com/what-is-a-blockchain),
  [Pt.2](https://unwttng.com/what-is-bitcoin-ethereum)

## Essentials

- [Bitcoin White Paper](https://bitcoin.org/bitcoin.pdf)
- [Ethereum White Paper](https://github.com/ethereum/wiki/wiki/White-Paper)

## Advanced

- [Ethereum Beige Paper](https://github.com/chronaeon/beigepaper/blob/master/beigepaper.pdf),
  [Yellow Paper](https://ethereum.github.io/yellowpaper/paper.pdf)
- [How does Ethereum work, anyway?](https://medium.com/@preethikasireddy/how-does-ethereum-work-anyway-22d1df506369)
- [DAOs, DACs, DAs and More: An Incomplete Terminology Guide](https://blog.ethereum.org/2014/05/06/daos-dacs-das-and-more-an-incomplete-terminology-guide/)
- [How To Learn Solidity: The Ultimate Ethereum Coding Tutorial](https://blockgeeks.com/guides/solidity/)
- [JSON-RPC protocol](https://en.wikipedia.org/wiki/JSON-RPC), [JSON-RPC API](https://github.com/ethereum/wiki/wiki/JSON-RPC#json-rpc-api)

## Background

- Protocols vs. Software [Pt.1](https://medium.com/clearmatics/protocols-vs-software-part-1-c0662ff7a4c),
  [Pt.2](https://medium.com/clearmatics/protocol-vs-software-part-2-protocol-approach-for-resilient-distributed-systems-ff99ce67c738)
- [Merkle tree](https://en.wikipedia.org/wiki/Merkle_tree)
- [How Log Proofs Work](http://www.certificate-transparency.org/log-proofs-work)
- [Merkling in Ethereum](https://blog.ethereum.org/2015/11/15/merkling-in-ethereum/)
- [Hash-based Signatures: An Illustrated Primer](https://blog.cryptographyengineering.com/2018/04/07/hash-based-signatures-an-illustrated-primer/)
