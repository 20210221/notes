# Singing Notes

- **Key**: the right amount of closure, i.e. optimal airflow

## Vocal exercises

- Yawning (throat warm-up)
- Hissing ('sss' sound)
- Lip rolling ("motorboat")
- Raspberry trill (lip rolling with tongue in between lips, soft → strong → soft)
- Humming ('mmm' sound)
- Yah-yah (tongue on the lower lip, fixed jaw)
- Mom-mom (crying effect)
- Nghe-nghe (whining effect)
- Uh-uh (staccato, jumping from note to note)
- Slide (an octave up and back slowly)
- Vocal fry / siren (slide all the range)

## Position

- Stand with your back straight
- Hold your head straight (no tension in the neck)
- Don't raise your shoulders when breathing in
- Open your mouth wide
- Forcing a smile helps with correct voice placement

## Tips

- Imagine breathing into your pelvis
- Imagine singing to a fixed remote object
- Slide to the centre of pitch from above, not from below
- When you're singing in the high end of your range, think low
- When you're singing in the low end of your range, think high
